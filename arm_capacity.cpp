#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

//#include "classifier_arm.h"
#include "graphTopology.h"

using namespace std;

//boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
//boost::shared_ptr<Flow_arm> flow_arm;
//boost::shared_ptr<Flow_thumb> flow_thumb;
extern boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
extern boost::shared_ptr<APE_ARM> APE_classifier;

string folder = "test/arm/";
vector<string> dirs; // directories
string buffer; // main buffer for test
vector<Classifier*> classifiers;

GraphTopology *topology;
/*
string ts()
{
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	char * cur_time = asctime(timeinfo);
	cur_time[strlen(cur_time)-1] = '\0';
	return cur_time;
};
*/
int timeval_subtract_ARM(struct timeval *result, struct timeval *x, struct timeval *y)
{
	// Perform the carry for the later subtraction by updating y. 
	if (x->tv_usec < y->tv_usec) 
	{
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	
	if (x->tv_usec - y->tv_usec > 1000000) 
	{
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}
	 
	// Compute the time remaining to wait. tv_usec is certainly positive.
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;
	 
	// Return 1 if result is negative.
	return x->tv_sec < y->tv_sec;
}

int timeval_addition_ARM(struct timeval *result, struct timeval *x, struct timeval *y)
{
	result->tv_sec = x->tv_sec + y->tv_sec;
	result->tv_usec = x->tv_usec + y->tv_usec;
	if (result->tv_usec > 1000000)
	{
		result->tv_sec++;
		result->tv_usec = result->tv_usec-1000000;
	}
	return 0;
}
/*
int aza_main()
{
	gettimeofday(&startTime, NULL);
	bool mal = false;
	current->update();
	mal = current->check();

	gettimeofday(&endTime, NULL);
}
*/
int main()
{
	long BUFFER_SIZE = MAX_DATA_LEN;//256;
	fstream file;
	DIR *dir;
    struct dirent *entry;
	long checked_buffers=0, fileNum=0;
	string filename;
	char ch;
	struct timeval startTime, endTime, resultTime, spentTime;
	double time_s, capacity_bps;
	double capacity_sum = 0;
	int dir_count = 0;
		
	//buffer = string( BUFFER_SIZE, '\x00' );
	
	// --- HYBRID ---
	topology = new GraphTopology();
	// STATIC
	//topology->addStaticClassifier_ARM( new DisasLength_ARM() );
	//topology->addStaticClassifier_ARM( new DisasLength_APE_ARM() );
	topology->addStaticClassifier_ARM( new Call_patterns_init_APE_ARM() );
	//topology->addStaticClassifier_ARM( new APE_flag_check_ARM() );
	topology->addStaticClassifier_ARM( new Decrypt_init_APE_ARM() );
	topology->addStaticClassifier_ARM( new Mode_change_APE_ARM() );
	
	//topology->addStaticClassifier_ARM( new Stride_ARM() );
	
	// DYNAMIC
	topology->addDynamicClassifier_ARM( new wxNum_EmulBased_ARM() );
	topology->addDynamicClassifier_ARM( new decrypt_EmulBased_ARM() );
	topology->addDynamicClassifier_ARM( new branchWrited_EmulBased_ARM() );
	//topology->addDynamicClassifier_ARM( new modeChanged_EmulBased_ARM() );
	topology->addDynamicClassifier_ARM( new malicCall_EmulBased_ARM() );
	
	// -- LINEAR ---
	classifiers.push_back( new DisasLength_ARM() );
	classifiers.push_back( new DisasLength_APE_ARM() );
	classifiers.push_back( new Call_patterns_init_APE_ARM() );
	classifiers.push_back( new Decrypt_init_APE_ARM() );
	classifiers.push_back( new Mode_change_APE_ARM() );
	
	classifiers.push_back( new Stride_ARM() );
	
	classifiers.push_back( new wxNum_EmulBased_ARM() );
	//classifiers.push_back( new unReadNum_EmulBased_ARM() );
	classifiers.push_back( new decrypt_EmulBased_ARM() );
	classifiers.push_back( new branchWrited_EmulBased_ARM() );
	//classifiers.push_back( new modeChanged_EmulBased_ARM() );
	classifiers.push_back( new malicCall_EmulBased_ARM() );
	
	vector<long> rate_classifier( classifiers.size() );
	
	//dirs.push_back( "legitimate" );
	//dirs.push_back( "shellcode/plain" );
	//dirs.push_back( "shellcode/crypt" );
	//dirs.push_back( "multimedia" );
	//dirs.push_back( "random" );
	
	dirs.push_back( "_mix/mixed_shell_rand" );
	dirs.push_back( "_mix/mixed_legitimate" );
	dirs.push_back( "multimedia" );
	dirs.push_back( "random" );
		
	// DIRECTORY
	for(int i=0; i<dirs.size(); i++)
	{
		capacity_sum = 0;
		dir_count = 0;
	
		for (int k=0; k<rate_classifier.size(); k++)
			rate_classifier[k]=0;
		
		dir = opendir( (folder+dirs[i]).c_str() );
		if (!dir) {
			cerr << "Open directory error " << endl;
			exit(1);
		};
		//*Directory name
		cout << "***** "<< dirs[i].c_str() <<" *****" << endl;

		checked_buffers=0;
		fileNum=0;
		// FILES
		while ( ( (entry = readdir(dir)) != NULL ) && dir_count <= 10) 
		//entry = readdir(dir);
		{
		  // IF "." ".."
		  if ( strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0 )
		  {
		    //*Filename
			//cout << "/" << dirs[i]+"/"+entry->d_name << endl;
			
			filename = folder+dirs[i]+"/"+entry->d_name;
			file.open( filename.c_str(), ios::in | ios::binary );
			if ( file.fail() )
			{ 
			   cerr << "Open error "<<filename << endl; 
			   exit(1); 
			}
			fileNum++;
			
			// FULL FILE
			while( !file.eof() && dir_count <= 10 )
			{
				dir_count++;
				buffer.clear();
				//for( int k=0; !file.eof(); k++) // ALL FILE
				for( int k=0; k<MAX_DATA_LEN && !file.eof(); k++) // PER BUFFER
				{
					file.get(ch);
					buffer.push_back(ch);
				}/*
				FlowBased_ARM::init( (unsigned const char*)buffer.c_str(), buffer.size() );
				checked_buffers++;*/
				
				timerclear(&spentTime);
				gettimeofday(&startTime, NULL);
				for (int w=0; w<1000; w++)
				{
					topology->executeTopology( (unsigned const char*)buffer.c_str(), buffer.size(), true, true );
				}
				gettimeofday(&endTime, NULL);
				timersub(&endTime, &startTime, &spentTime);
				
				time_s = ((double)spentTime.tv_sec*1000000.0 + (double)spentTime.tv_usec) /1000000.0;
				capacity_bps = (double)BUFFER_SIZE*1000.0 / time_s;
				cout << "Capacity = "<<(capacity_bps/1048576.0)*8.0<<" Mbit/s"<<endl;
				capacity_sum += (capacity_bps/1048576.0)*8.0;
				
				/*
				FlowBased_ARM::init( (unsigned const char*)buffer.c_str(), buffer.size() );
				timerclear(&spentTime);
				gettimeofday(&startTime, NULL);
				for (int w=0; w<1000; w++)
				{
					APE_classifier->APE_check();
				}
				gettimeofday(&endTime, NULL);
				timersub(&endTime, &startTime, &spentTime);
				
				time_s = ((double)spentTime.tv_sec*1000000.0 + (double)spentTime.tv_usec) /1000000.0;
				capacity_bps = (double)BUFFER_SIZE*1000.0 / time_s;
				cout << "Capacity = "<<(capacity_bps/1048576.0)*8.0<<" Mbit/s"<<endl;
				FlowBased_ARM::release();
				*/
				
				//* SHOW COUNTER
				//cout << "\r**Buffers scanned = "<<checked_buffers<<" | Files = "<<fileNum;
				/*
				timerclear(&spentTime);
				gettimeofday(&startTime, NULL);
				for (int w=0; w<1000; w++)
				{
					FlowBased_ARM::init( (unsigned const char*)buffer.c_str(), buffer.size() );
					for ( int j=0; j<classifiers.size(); j++ )
					{
						if ( classifiers[j]->check() )
							rate_classifier[j] ++;
					}
					FlowBased_ARM::release();
				}
				gettimeofday(&endTime, NULL);
				timersub(&endTime, &startTime, &spentTime);
				
				time_s = ((double)spentTime.tv_sec*1000000.0 + (double)spentTime.tv_usec)/1000000.0;
				capacity_bps = (double)BUFFER_SIZE*1000.0 / time_s;
				cout << "Capacity = "<<(capacity_bps/1048576.0)*8.0<<" Mbit/s"<<endl;
				*/
				//FlowBased_ARM::release();
				/*
				timerclear(&spentTime);
				gettimeofday(&startTime, NULL);
				for (int w=0; w<1000; w++)
				{
					FlowBased_ARM::init_only( (unsigned const char*)buffer.c_str(), buffer.size() );
					FlowBased_ARM::release();
				}
				gettimeofday(&endTime, NULL);
				timersub(&endTime, &startTime, &spentTime);
				time_s = spentTime.tv_usec/1000000.0;
				//time_s = ((double)spentTime.tv_sec*1000000.0 + (double)spentTime.tv_usec) /1000000.0;
				capacity_bps = (double)BUFFER_SIZE*1000.0 / time_s;
				cout << "Capacity = "<<(capacity_bps/1048576.0)*8.0<<" Mbit/s"<<endl;
				*/
				
			}// FILE
			file.close();
			
		  }// IF "." ".."
		  
		};// FILES

		closedir(dir);
		
		cout << endl << "Capacity summary = "<< capacity_sum/10.0 <<" Mbit/s" << endl <<endl;
		cout<<endl;
		/*
		for (int k=0; k<rate_classifier.size(); k++)
		{
			classifiers[k]->printInfo(); 
			cout << "= ";
			cout << rate_classifier[k] << endl;
		}
		cout<<endl;*/
		
	}// DIRECTORY
}
