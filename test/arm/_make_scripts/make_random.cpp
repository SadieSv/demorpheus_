#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h> 

#define FILE_SIZE 20000000

using namespace std;

int main()
{
	FILE* f; 
	
	srand (time(NULL));
	f = fopen( "random", "w");
	
	for ( long i=0; i<FILE_SIZE; i++)
	{
		char c = rand() % 256;
		fprintf(f, "%c", c );
		
	}
	
	fclose(f);
}