#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "classifier_arm.h"

using namespace std;

//boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
//boost::shared_ptr<Flow_arm> flow_arm;
//boost::shared_ptr<Flow_thumb> flow_thumb;
extern boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
extern boost::shared_ptr<APE_ARM> APE_classifier;

string folder = "test/arm/";
vector<string> dirs; // directories
string buffer; // main buffer for test
vector<Classifier*> classifiers;
Armulator* emul;

int main()
{
	long BUFFER_SIZE = MAX_DATA_LEN;
	fstream file;
	DIR *dir;
    struct dirent *entry;
	long checked_buffers=0, fileNum=0;
	string filename;
	char ch;
	int max_arm_len, max_thumb_len, max_all_len;
	int max_arm_len_APE, max_thumb_len_APE, max_all_len_APE;
		
	//buffer = string( BUFFER_SIZE, '\x00' );
	/*
	classifiers.push_back( new DisasLength_ARM() );
	classifiers.push_back( new DisasLength_APE_ARM() );
	classifiers.push_back( new Call_patterns_init_APE_ARM() );
	classifiers.push_back( new BX_patterns_init_APE_ARM() );
	classifiers.push_back( new Decrypt_init_APE_ARM() );
	classifiers.push_back( new Get_Use_PC_APE_ARM() );
	classifiers.push_back( new Mode_change_APE_ARM() );
	classifiers.push_back( new Stride_ARM_instr() );
	classifiers.push_back( new Stride_ARM_byte() );
	*/
	vector<long> rate_classifier( classifiers.size() );
	
	//dirs.push_back( "legitimate" );
	//dirs.push_back( "shellcode/plain" );
	//dirs.push_back( "shellcode/crypt" );
	//dirs.push_back( "multimedia" );
	//dirs.push_back( "random" );
	dirs.push_back( "test" );
		
	// DIRECTORY
	for(int i=0; i<dirs.size(); i++)
	{
		for (int k=0; k<rate_classifier.size(); k++)
			rate_classifier[k]=0;
		
		dir = opendir( (folder+dirs[i]).c_str() );
		if (!dir) {
			cerr << "Open directory error " << endl;
			exit(1);
		};
		//*Directory name
		cout << "***** "<< dirs[i].c_str() <<" *****" << endl;

		checked_buffers=0;
		fileNum=0;
		// FILES
		while ( (entry = readdir(dir)) != NULL) 
		//entry = readdir(dir);
		{
		  // IF "." ".."
		  if ( strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0 )
		  {//printf("%d - %s [%d] %d\n", entry->d_ino, entry->d_name, entry->d_type, entry->d_reclen);
		  
		    //*Filename
			cout << "/" << dirs[i]+"/"+entry->d_name << endl;
			
			filename = folder+dirs[i]+"/"+entry->d_name;
			file.open( filename.c_str(), ios::in | ios::binary );
			if ( file.fail() )
			{ 
			   cerr << "Open error "<<filename << endl; 
			   exit(1); 
			}
			fileNum++;
			//cout << "\r"<<fileNum;
			
			// FULL FILE
			while( !file.eof() )
			{
				buffer.clear();
				//for( int k=0; !file.eof(); k++) // ALL FILE
				for( int k=0; k<MAX_DATA_LEN && !file.eof(); k++) // PER BUFFER
				{
					//file >> buffer[k];
					file.get(ch);
					//file >> ch;
					//printf("\\x%02x ", ch);
					buffer.push_back(ch);
				}//cout<<endl;
				//cout<<buffer.size()<<endl;
				//FlowBased_ARM::init( (unsigned const char*)buffer.c_str(), buffer.size() );
				checked_buffers++;

				//* SHOW COUNTER
				//cout << "\r**Buffers scanned = "<<checked_buffers<<" | Files = "<<fileNum;
				
				// MAIN CLASSIFIER CHECK
				
				emul = new Armulator;
				
				//if ( strcmp(entry->d_name,"thumb")==0 )
				//if ( strcmp(entry->d_name,"bxtest")==0 )
				{
					cout<<"*** ARM ***"<<endl;
					emul->execute( (unsigned const char*)buffer.c_str(), buffer.size() , 0, 0);
					cout<<"*** Thumb ***"<<endl;
					emul->execute( (unsigned const char*)buffer.c_str(), buffer.size() , 1, 0);
				}
				//delete emul;
			}// FILE
			file.close();
		  }// IF "." ".."
		  
		  
		};// FILES

		closedir(dir);
		cout<<endl;
		for (int k=0; k<rate_classifier.size(); k++)
		{
			classifiers[k]->printInfo(); 
			cout << "= ";
			//cout << ((double)rate_classifier[k]) / ((double)checked_buffers) << endl;
			cout << rate_classifier[k] << endl;
		}
		cout<<endl;
		
		
	}// DIRECTORY
	
	
	/*
	file.open( "file", ios::in | ios::binary );
	while( !file.eof() )
	{
		
		for( int i=0; i<BUFFER_SIZE; i++)
			f >> buffer[i];
		
		
		
	}
	f.close();
	*/
	
	
	
}