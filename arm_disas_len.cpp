#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "classifier_arm.h"

using namespace std;

//boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
//boost::shared_ptr<Flow_arm> flow_arm;
//boost::shared_ptr<Flow_thumb> flow_thumb;
extern boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
extern boost::shared_ptr<APE_ARM> APE_classifier;

string folder = "test/arm/";
vector<string> dirs; // directories
string buffer; // main buffer for test
vector<Classifier*> classifiers;
vector<int> sizes;

int main()
{
	long BUFFER_SIZE = MAX_DATA_LEN;
	fstream file;
	DIR *dir;
    struct dirent *entry;
	long checked_buffers=0, fileNum=0;
	string filename;
	char ch;
	int max_arm_len=0, max_thumb_len=0, max_all_len=0;
	double mid_arm_len=0, mid_thumb_len=0, mid_all_len=0;
	int arm_Instruction_Length, thumb_Instruction_Length;
	
	APE_flow_info tmp1;
	bitset<16> regs;
	bitset<16> regs_has_pc;
		
	//buffer = string( BUFFER_SIZE, '\x00' );
	
	DisasLength_ARM disas_len;
	/*
	classifiers.push_back( new DisasLength_ARM() );
	classifiers.push_back( new DisasLength_APE_ARM() );
	classifiers.push_back( new Call_patterns_init_APE_ARM() );
	classifiers.push_back( new BX_patterns_init_APE_ARM() );
	classifiers.push_back( new Decrypt_init_APE_ARM() );
	classifiers.push_back( new Get_Use_PC_APE_ARM() );
	classifiers.push_back( new Mode_change_APE_ARM() );
	classifiers.push_back( new Stride_ARM_instr() );
	classifiers.push_back( new Stride_ARM_byte() );
	
	classifiers.push_back( new wxNum_EmulBased_ARM() );
	classifiers.push_back( new unReadNum_EmulBased_ARM() );
	classifiers.push_back( new unWriteNum_EmulBased_ARM() );
	classifiers.push_back( new branchWrited_EmulBased_ARM() );
	classifiers.push_back( new modeChanged_EmulBased_ARM() );
	classifiers.push_back( new malicCall_EmulBased_ARM() );
	*/
	
	sizes.push_back(128);
	sizes.push_back(256);
	sizes.push_back(512);
	sizes.push_back(1024);
	sizes.push_back(1400);
	
	vector<long> rate_classifier( classifiers.size() );
	
	dirs.push_back( "_mix/random_2" );
	dirs.push_back( "_mix/mixed_shell_rand" );
	
	for ( int i=0; i<sizes.size(); i++ )
	{
		BUFFER_SIZE = sizes[i];
		
		for ( int fold=0; fold<2; fold++ )
		{
			filename = folder + dirs[fold] + "/" + "mix" + std::to_string(sizes[i]);
			file.open( filename.c_str(), ios::in | ios::binary );
			
			// FILENAME
			cout<<endl;
			cout << "*****" << filename << endl;
			
			checked_buffers = 0;
			mid_arm_len=0; mid_thumb_len=0; mid_all_len=0;
			while( !file.eof() )
			{
				max_arm_len=0; max_thumb_len=0; max_all_len=0;
			
				buffer.clear();
				for( int k=0; k<BUFFER_SIZE && !file.eof(); k++)
				{
					file.get(ch);
					buffer.push_back(ch);
				}
				FlowBased_ARM::init( (unsigned const char*)buffer.c_str(), buffer.size() );
				checked_buffers++;
				
				//* SHOW COUNTER
				//cout << "\r**Buffers scanned = "<<checked_buffers;
				
				// SIMPLE DISAS LEN
				/*
				for (unsigned k = 0; k < flow_ARM->at(0)->fl.size(); k++ )
				{
					arm_Instruction_Length = disas_len.getChainSize_arm( k, 0 );
					//cout<<"i= "<<k<<" | size= "<<arm_Instruction_Length<<endl;			
					if ( arm_Instruction_Length > max_arm_len ) 
						max_arm_len = arm_Instruction_Length;
				}// ARM
				
				for (unsigned k = 0; k < flow_ARM->at(1)->fl.size(); k++ )
				{
					thumb_Instruction_Length = disas_len.getChainSize_thumb( k, 0 );
					//cout<<"i= "<<k<<" | size= "<<thumb_Instruction_Length<<endl;					
					if ( thumb_Instruction_Length > max_thumb_len )
						max_thumb_len = thumb_Instruction_Length;
				}// THUMB
				*/
				
				// APE DISAS LEN
				
				regs.reset();
				regs_has_pc.reset();
				regs[13] = regs[15] = 1;
				//---ARM---
				for (int i=0; i<flow_ARM->at(0)->fl.size(); i++)
				{
					tmp1 = APE_classifier->forward_flow(i,0,ARM_MODE,regs,regs_has_pc);
					
					arm_Instruction_Length = tmp1.max_disas_len;
					if ( arm_Instruction_Length > max_arm_len ) 
						max_arm_len = arm_Instruction_Length;
				}
				//---THUMB---
				for (int i=0; i<flow_ARM->at(1)->fl.size(); i++)
				{
					tmp1 = APE_classifier->forward_flow(i,0,THUMB_MODE,regs,regs_has_pc);
					
					thumb_Instruction_Length = tmp1.max_disas_len;
					if ( thumb_Instruction_Length > max_thumb_len )
						max_thumb_len = thumb_Instruction_Length;
				}
				
				
				mid_arm_len += max_arm_len;
				mid_thumb_len += max_thumb_len;
				
				FlowBased_ARM::release();
				
			}// WHILE
			
			mid_arm_len /= (double)checked_buffers;
			mid_thumb_len /= (double)checked_buffers;
			
			cout<<endl;
			cout<<"mid_arm_len= "<<mid_arm_len<<endl;
			cout<<"mid_thumb_len= "<<mid_thumb_len<<endl;
			
			file.close();
		}// FOR FOLDERS
	}// FOR FILES
	
}





