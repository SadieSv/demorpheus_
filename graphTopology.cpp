#include <iostream>
#include <vector>
#include <queue>

#include "macros.h"
#include "graphTopology.h"


using namespace std;

/**
 * Subtract the timeval values x and y, storing the result in result param
 * @param result	-	here the value of difference will be stored
 * @param x			-	parameter for subtraction
 * @param y			-	difference
 * @return	1 if the difference is negative
 * 			0 otherwise
 */
int timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y)
{
	// Perform the carry for the later subtraction by updating y. 
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }
     
    // Compute the time remaining to wait. tv_usec is certainly positive.
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;
     
    // Return 1 if result is negative.
    return x->tv_sec < y->tv_sec;
}

int timeval_addition(struct timeval *result, struct timeval *x, struct timeval *y)
{
	result->tv_sec = x->tv_sec + y->tv_sec;
	result->tv_usec = x->tv_usec + y->tv_usec;
	if (result->tv_usec > 1000000){
		result->tv_sec++;
		result->tv_usec = result->tv_usec-1000000;
	}
	return 0;
}

double defaultQuality(double fp, double fn, double complexity)
{
    return (double)((fp + 1.0)*(fn+ 1.0)/complexity);
}

GraphTopology::GraphTopology()
{
	last_time_.tv_sec = 0;
	last_time_.tv_usec = 0;
	
	Classifier::init();
	FlowBased::init();
	CFGBased::init();
	
	topology_.clear();
	mClassifier = new InitialLength();
}

GraphTopology::~GraphTopology()
{
	for (int i=0; i<elem_classifiers_.size(); i++)
		if(elem_classifiers_[i]) delete elem_classifiers_[i];
	CFGBased::release();
}

void GraphTopology::addClassifier(Classifier* classifier)
{
    elem_classifiers_.push_back(classifier);
}

void GraphTopology::addStaticClassifier_ARM(Classifier* classifier)
{
	static_classifiers_ARM.push_back(classifier);
}

void GraphTopology::addDynamicClassifier_ARM(Classifier* classifier)
{
	dynamic_classifiers_ARM.push_back(classifier);
}

void GraphTopology::clearDetectedClasses(char& detected)
{
    detected = 0;
}

void  GraphTopology::recognizeShellcodeClasses(const vector<Classifier*> classifiers, char & detected)
{
    for(int i=0; i< classifiers.size(); i++)
      detected = detected | classifiers[i]->types_;
}

bool GraphTopology::isSameClasses(const Classifier* classifier, char detected)
{
    if (!classifier) return false;
    
    return (classifier->types_ == detected);
}

void GraphTopology::classesLeftToRecognize(const Classifier* classifier, char detected, char& new_detected)
{
    if(!classifier)
    {
	new_detected = detected;
	return;
    }
    
    new_detected = classifier->types_ ^ detected;
}

bool GraphTopology::isOverlapped(const Classifier* first_classifier, const Classifier* second_classifier)
{
    return ( (first_classifier->types_ & second_classifier->types_) !=0 );
}

void GraphTopology::makeCombinations(const vector<Classifier*> classifiers, char detected, 
				     std::vector< vector<Classifier*> > &level)
{
  for (int i=0; i< classifiers.size(); i++)
  {
      if(!isSameClasses(classifiers[i], detected))
      {
	if (classifiers.size() > 1 )
	{
	    vector<Classifier*> reduced_vector;
	    reduced_vector.insert(reduced_vector.end(), classifiers.begin()+i+1, classifiers.end());
	    
	    char new_detected;
	    classesLeftToRecognize(classifiers[i], detected, new_detected);
	    
	    vector< vector<Classifier*> > new_level;
	    makeCombinations(reduced_vector, new_detected, new_level);
	    for (int j=0; j< new_level.size(); j++)
	    {
		new_level[j].push_back(classifiers[i]);
	    }
	    level.insert(level.end(), new_level.begin(), new_level.end());
	}
      }
      //if considered classifier detects all detectable clases
      else
      {
	vector<Classifier*> current_vector;
	current_vector.push_back(classifiers[i]);
	level.push_back(current_vector);
      }
  }
}

double GraphTopology::levelQuality(const vector<Classifier*> level, double (*quality)(double, double, double))
{
    double fn = 0.0;
    double fp = 0.0;
    double complexity = 0.0;
    
    for (int i=0; i< level.size(); i++)
    {
	if (fn < level[i]->getFN()) fn = level[i]->getFN();
	if (fp < level[i]->getFPR()) fp = level[i]->getFPR();
	complexity += level[i]->getComplexity();
    }
    return quality(fp, fn, complexity);  
}

vector<Classifier*> GraphTopology::selectOptimalLevel(const vector< vector<Classifier*> > &levels)
{
    double measure = 0.0;
    vector<Classifier*> combination;
    
    for(int i=0; i< levels.size(); i++)
    {
	double current_measure = levelQuality(levels[i]);
	if (current_measure > measure)
	{
	  measure = current_measure;
	  combination = levels[i];
	}
    }
    return combination;
}

void GraphTopology::linkLevels(const vector<Classifier*> previous, const vector<Classifier*> current)
{
    for (int i=0; i < previous.size(); i++)
    {
	for (int j=0; j< current.size(); j++)
	{
	    if (previous[i] == mClassifier || isOverlapped(previous[i], current[j]))
	    {
		map<Classifier*, vector<Classifier*> >::iterator ptr = topology_.find(previous[i]);
		if (ptr == topology_.end())
		{
		    PRINT_WARNING("incorrect topology: no previous layer detected in it");
		    return;
		}
		(*ptr).second.push_back(current[j]);
	    }
	}
    }
    return;
}

void GraphTopology::updateClassifiers(vector<Classifier*> &classifiers, const vector<Classifier*> level)
{
    vector<Classifier*>::const_iterator ptr;
    
    for(ptr = level.begin(); ptr != level.end(); ptr++)
    {
	for (int i=0; i< classifiers.size(); i++)
	{
	    if(classifiers[i] == (*ptr))
	    {
		classifiers.erase(classifiers.begin()+i);
		break;
	    }
	}
    }
    return;
}

void GraphTopology::makeHybridTopology()
{
	vector<Classifier*> classifiers = elem_classifiers_;
	
	vector<Classifier*> v;
	topology_.insert(pair<Classifier*, vector<Classifier*> > (mClassifier, v));
	
	vector<Classifier*> previous;
	previous.push_back(mClassifier);
	
	while(classifiers.size())
	{
	    char detected;
	    vector< vector<Classifier*> > levels;
	    
	    recognizeShellcodeClasses(classifiers, detected);
	    makeCombinations(classifiers, detected, levels);
	    vector<Classifier*> optimal = selectOptimalLevel(levels);
	    
	    linkLevels(previous, optimal);
	    updateClassifiers(classifiers, optimal);
	    
	    for (int i=0; i< optimal.size(); i++)
	    {
		topology_.insert(pair<Classifier*, vector<Classifier*> > (optimal[i], v));
	    }
	    previous = optimal;
	}
}
/*
void GraphTopology::makeHybridTopologyARM()
{
	vector<Classifier*> classifiers = elem_classifiers_;
	
	vector<Classifier*> v;
	topology_.insert(pair<Classifier*, vector<Classifier*> > (mClassifier, v));
	
	//vector<Classifier*> previous;
	previous.push_back(mClassifier);
	
	while(classifiers.size())
	{
	    char detected;
	    vector< vector<Classifier*> > levels;
	    
	    recognizeShellcodeClasses(classifiers, detected);
	    makeCombinations(classifiers, detected, levels);
	    vector<Classifier*> optimal = selectOptimalLevel(levels);
	    
	    linkLevels(previous, optimal);
	    updateClassifiers(classifiers, optimal);
	    
	    for (int i=0; i< optimal.size(); i++)
	    {
		topology_.insert(pair<Classifier*, vector<Classifier*> > (optimal[i], v));
	    }
	    previous = optimal;
	}
}*/

void GraphTopology::makeLinearTopology()
{
	ClassifierVertex vertex;
	std::vector<Classifier*> v;
	topology_.insert(pair<Classifier*, vector<Classifier*> > (mClassifier, v));
	for( int i=0; i< elem_classifiers_.size(); i++){
		//fills structure for the next execution
		markedClassifier.insert(std::pair<Classifier*, bool>(elem_classifiers_[i], false));
		
		//if first classifier, add it to initial classifier, 
		//which is already in topology_
		v.clear();
		if( i == elem_classifiers_.size()-1 ) 
			topology_.insert(std::pair<Classifier*, std::vector<Classifier*> >(elem_classifiers_[i], v) );
		else {
			v.push_back( elem_classifiers_[i+1] );
			topology_.insert(std::pair<Classifier*, std::vector<Classifier*> >(elem_classifiers_[i], v) );
		}
	}
}

void GraphTopology::clearMarkedClassifier()
{
	std::map<Classifier*, bool>::iterator ptr;
	for(ptr = markedClassifier.begin(); ptr != markedClassifier.end(); ptr++)
		(*ptr).second = false;
}

bool GraphTopology::executeTopology_ARM()
{
	bool static_success = false;
	bool dynamic_success = false;
	//cout<<"ARM detection"<<endl;
	FlowBased_ARM::init_disassemble();
	
	FlowBased_ARM::init_APE();
	for( int i=0; i<static_classifiers_ARM.size(); i++ )
	{
		if ( static_classifiers_ARM[i]->check() )
			static_success = true;
	}
	if (!static_success) 
	{//cout<<"ARM static fail\n";
		FlowBased_ARM::release();
		return false;
	}
	//else cout<<"ARM static win\n";
	
	FlowBased_ARM::init_emulator();
	for( int i=0; i<dynamic_classifiers_ARM.size(); i++ )
	{
		if ( dynamic_classifiers_ARM[i]->check() )
			dynamic_success = true;
	}
	if (!dynamic_success) 
	{//cout<<"ARM dynamic fail\n";
		FlowBased_ARM::release();
		return false;
	}
	//else cout<<"ARM dynamic win\n";
	FlowBased_ARM::release();
	return true;
}

//uses BFS graph algorithm
bool GraphTopology::executeTopology( const unsigned char* buffer, int length, bool reduce, bool arm )
{
	std::map<Classifier*, std::vector<Classifier*> >::iterator ptr;
	std::queue<Classifier*> executionOrder;
	std::map<Classifier*, bool>::iterator marked_ptr;
	Classifier *current;
	struct timeval startTime, endTime, diff, totalTime;
	
	Classifier::clearInformationVector();
	/*PRINT_DEBUG << "clear information vector!" << endl;
	if(DEBUG) {
		Classifier::printInformationVector();
	}*/
	Classifier::reload( buffer, length );
	
	// --- ARM mode ---
	if (arm) return executeTopology_ARM();
	
	FlowBased::reload();
	CFGBased::reload();

	//using copy of topology_ in case if there is need to cut some edges
	std::map<Classifier*, std::vector<Classifier* > > currenttopology_ = topology_;

	//initialize first classifier
	clearMarkedClassifier();
	executionOrder.push( mClassifier );
	
	//mark first classifier as explored
	marked_ptr = markedClassifier.find( mClassifier );
	if( marked_ptr == markedClassifier.end() ) {
		PRINT_WARNING(" Something wrong with markedClassifier structure");
		return false;
	}
	(*marked_ptr).second = true;
	totalTime.tv_sec = 0;
	totalTime.tv_usec = 0;
	
	while( !executionOrder.empty() ){
		//remove first classifier from the queue
		current = executionOrder.front();
		executionOrder.pop();
			
		ptr = currenttopology_.find( current );
		if ( ptr == currenttopology_.end() ) {
			PRINT_WARNING(" Something wrong with topology_ structure");
			return false;
		}
			
		bool mal = false;
		gettimeofday(&startTime, NULL);
		//execute first classifier
		current->update();
		mal  = current->check();
			
		gettimeofday(&endTime, NULL);
		timeval_subtract(&diff, &endTime, &startTime);
		timeval_addition(&totalTime, &totalTime, &diff);
		
		PRINT_DEBUG << "Classifier execution results" << endl;
		if (DEBUG) { current->printInfo(); cout << endl; }
		if( mal ) {
			current->updateInformationVector( Classifier::MALWARE );
			if (DEBUG) {
				Classifier::printInformationVector();
				PRINT_DEBUG << "malware" << endl <<endl;
			}
		}
		else {
			current->updateInformationVector( Classifier::LEGITIMATE );
			if (DEBUG) {
				Classifier::printInformationVector();
				PRINT_DEBUG << "legitimate" << endl << endl;
			}
			//if we are in hybrid topology, i.e. if we want to reduce the flow, we 
			//should break connection between current classifier and it's childrens
			PRINT_DEBUG << "reduce flag value: " << reduce << endl;
			if( reduce )
				(*ptr).second.clear(); //OMFG!!
		}
			
		//look for a children
		for( int i=0; i< (*ptr).second.size(); i++ ) {
			marked_ptr = markedClassifier.find( (*ptr).second[i] );
			if( marked_ptr == markedClassifier.end())  {
				PRINT_WARNING(" Something wrong with markedClassifier structure");
				return false;
			}
			if( (*marked_ptr).second ) continue;
			(*marked_ptr).second = true;
			executionOrder.push((*marked_ptr).first);
		}
	}
	
	last_time_ = totalTime;
	PRINT_DEBUG << "check information vector " << Classifier::checkInformationVector() << std::endl;
	if(DEBUG) Classifier::printInformationVector();
	return Classifier::checkInformationVector();
}


void GraphTopology::printTopology()
{
	std::map<Classifier*, std::vector<Classifier*> >::iterator ptr; 
	
	
	if (!topology_.size() ) 
	  
	{
	  cout << "nothing to print" << endl;
	  return;
	}

	for( ptr = topology_.begin(); ptr != topology_.end(); ptr++) {
		for( int j=0; j< (*ptr).second.size(); j++ ){
			(*ptr).first->printInfo();
			cout<< " ---> ";
			(*ptr).second[j]->printInfo();
			cout<< std::endl;
		}
	}
	

}

string GraphTopology::printClasses()
{
	return Classifier::printClasses();
}