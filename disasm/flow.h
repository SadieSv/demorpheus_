#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"
#pragma once
#include <vector>
#include <map>
#include <set>
#include <string>

//#define MAX_DATA_LEN 256
#define MAX_DATA_LEN 1408
#define MIN_DATA_LEN 64

#ifdef LIBDASM
#include "libdasm.h"
#endif
#include <udis86.h>

using namespace std;

#ifdef LIBDASM
struct oper
{
        enum Operand type;
        int reg;
        DWORD immediate;
};
#endif

struct x86_instruction
{
	int size;
	int type;//:8;
	int offset;
	bool ext_off;
	char *str;
	//string str;
	#ifdef LIBDASM
	oper op1;
	oper op2;
	#endif 
};

typedef std::vector< std::vector< struct x86_instruction > > fl_type;

//TODO: fix Flow class ( make data fields private )
class Flow
{
private:
	/**function disassembles flow and fills fl field
	 * @param buffer - given source buffer
	 * @param length - length of buffer
	 * @param offset - offset from with to start disassembling
	 */
	void disassemble_flow( const unsigned char *buffer , unsigned length, int off);
	void disassemble_flow_udis86(const unsigned char *buffer, unsigned length, int off);
	
	/** initialize offsets and ext_offset array */
	void init();
	fl_type fl;
public:
	Flow();
	Flow(const unsigned char* buffer, int length );
	
	fl_type flow;

	int getChainSize(int i);
	int getInstructionOffset(int i, int j);
	bool getExternalOffset(int i);
	int getChainNumber(int i);
	int getChainOffset(int i);
	int getFlowSize() {return fl.size();};
	//first item in pair stands for chain number, second for offset in the chain
	std::pair< int, int > offsets[MAX_DATA_LEN];
	bool ext_offset[MAX_DATA_LEN];

//additional function
	void insertChildAndParent( int o, int addr );
	bool doJmp( int o, int addr );
	Flow& generateEIFG();

//dubugging function
	void printFlow();
	void print(string s);
};



#pragma clang diagnostic pop