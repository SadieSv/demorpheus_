#include <vector>
#include <map>
#include <iostream>
#include <set>

#include "flow.h"
#include "inst_types.h"
#include "libdasm.h"

#define MAX_INSTRUCTION_LENGTH 16
#define  MODE MODE_32

using namespace std;

void Flow::init()
{
		for( int i=0; i < MAX_DATA_LEN; i++){
			offsets[i].first = -1;
			ext_offset[i] = false;
		}
}

Flow::Flow()
{
	init();
}
Flow::Flow( const unsigned char * buffer, int length )
{
	int o = 0;
	
	init();
	
	while ( length > 0 ){
		//if we didn't disassemble from such offset yet
                if( offsets[o].first == -1 )
				//disassemble_flow( buffer+o, length, o );
                    disassemble_flow_udis86(buffer + o, (unsigned int) length, o);
				o += 1;
				length -= 1;

	}
	return;
}


void Flow::disassemble_flow_udis86(const unsigned char *buffer, unsigned length, int off)
{
	struct x86_instruction ins;
	ud_t ud_obj;
	ud_init(&ud_obj);
	ud_set_mode(&ud_obj, 32);
	ud_set_syntax(&ud_obj, UD_SYN_ATT);
	
	ud_set_input_buffer(&ud_obj, buffer, length);

    //cout << "buffer lenght: " << length << endl;
	while (int l = ud_disassemble(&ud_obj))
	{
		int off_tmp = fl.size() - 1; // the last chain position in the flow
		int offset = ud_insn_off(&ud_obj);
		//offset met for first time
		if(offsets[ off + offset ].first < 0 )
		{
			ud_mnemonic_code mc = ud_insn_mnemonic(&ud_obj);
			if (mc == UD_Iinvalid )
			{
				return;
			}
			
			ins.size = ud_insn_len(&ud_obj);
			ins.offset = ud_insn_off(&ud_obj) + off;
			ins.ext_off = false;
			ins.str = const_cast<char*>(ud_insn_asm(&ud_obj));
			
			if (mc == UD_Ijo || mc == UD_Ijno || mc == UD_Ijb || mc == UD_Ijae || mc == UD_Ijz
				|| mc == UD_Ijnz || mc == UD_Ijbe || mc == UD_Ija || mc == UD_Ijs || mc == UD_Ijns ||
				mc == UD_Ijp || mc == UD_Ijnp || mc == UD_Ijl || mc == UD_Ijge || mc == UD_Ijle || 
				mc == UD_Ijg || mc == UD_Ijcxz || mc == UD_Ijecxz || mc == UD_Ijrcxz)
			{
				ins.type = INSTRUCTION_TYPE_JMPC;
			}
			else if (mc == UD_Ijmp) ins.type = INSTRUCTION_TYPE_JMP;
            else if (   mc == UD_Icmovo || mc == UD_Icmovno || mc == UD_Icmovb || mc == UD_Icmovae ||
                        mc == UD_Icmovz || mc == UD_Icmovnz || mc == UD_Icmovbe || mc == UD_Icmova ||
                        mc == UD_Icmovl || mc == UD_Icmovge || mc == UD_Icmovle  ||
                        mc == UD_Icmovs  || mc == UD_Icmovns  || mc == UD_Icmovp  || mc == UD_Icmovnp)
                ins.type = INSTRUCTION_TYPE_MOVC;
            else if (   mc == UD_Imov || mc == UD_Imovapd || mc == UD_Imovaps|| mc == UD_Imovd || mc == UD_Imovhpd  ||
                        mc == UD_Imovhps || mc == UD_Imovlhps  || mc == UD_Imovlpd ||
                        mc == UD_Imovmskpd || mc == UD_Imovmskps || mc == UD_Imovntdq || mc == UD_Imovnti ||
                        mc == UD_Imovntpd || mc == UD_Imovq || mc == UD_Imovzx )
                ins.type = INSTRUCTION_TYPE_MOV;
			else ins.type = INSTRUCTION_TYPE_OTHER;
			
			//TODO: fill operand types
			
			if ( !offset )
			{
				std::vector<struct x86_instruction> tmp;
				tmp.push_back( ins );
				fl.push_back( tmp );
				offsets[ off + offset ].first = off_tmp + 1;
				offsets[ off + offset ].second = fl[ off_tmp + 1 ].size()-1;
			}
			else {
				fl[ off_tmp ].push_back( ins );
				offsets[ off + offset ].first = off_tmp;
				offsets[ off + offset ].second = fl[off_tmp].size() - 1;
			}
		}
		//offset have been already met
		else
		{
			ins.offset = off + offset;
			ins.ext_off = true;
			ext_offset[ off + offset ] = true;
			fl[ fl.size() -1 ].push_back( ins );
			return;
		}
	}
	
	return;
}


void Flow::disassemble_flow( const unsigned char * buffer, unsigned length, int off )
{
    INSTRUCTION inst;
	int o = 0;
	char buf[300];
	struct x86_instruction ins;

	while( length > 0 && off + o < MAX_DATA_LEN )
	{		
		//if we didn't disassemble from such offset yet
		if ( offsets[off+o].first == -1 )
		{
			BYTE *addr = const_cast<BYTE*>( buffer+o );
			int ins_len = get_instruction(&inst, addr, MODE);
			
			int off_tmp = fl.size()-1; // the last chain position in the flow

			//filling instruction field
			ins.size = ins_len;
			ins.type = inst.type;
			ins.offset = off+o;
			ins.ext_off = false;

            #ifdef LIBDASM
			ins.op1.type = inst.op1.type;
			ins.op2.type = inst.op2.type;
			ins.op1.reg = inst.op1.reg;
			ins.op2.reg = inst.op2.reg;
			ins.op1.immediate = inst.op1.immediate;
			ins.op2.immediate = inst.op2.immediate;
			#endif

			//if it's first instruction in the given buffer, create new chain in the flow
			//otherwise insert instruction in the end of last chain
			if ( o == 0 ){
				std::vector<struct x86_instruction> tmp;
				tmp.push_back( ins );
				fl.push_back( tmp );
				
				offsets[off+o].first = off_tmp+1;
				offsets[off+o].second = fl[ off_tmp+1 ].size()-1;
				
			}
			else {
				fl[ off_tmp ].push_back( ins );
				offsets[off+o].first = off_tmp;
				offsets[ off+o].second = fl[off_tmp].size()-1;
			}
			length -= ins_len;
			o += ins_len;
		}
		//if we've already met instruction with given offset then create reference element to other 
        // instruction in the other chain ( position of target instruction stored in the offsets vector)
		else {
			ins.offset = off+o;
			ins.ext_off = true;
			ext_offset[off+o] = true;
			fl[ fl.size() -1 ].push_back( ins );
		return;
		}
	}
    return ;

}

int Flow::getChainSize(int i)
{
	if (i >= fl.size() ) return -1;
	else return fl[i].size();
}

int Flow::getInstructionOffset(int i, int j)
{
	return fl[i][j].offset;
}

bool Flow::getExternalOffset(int i)
{
	return ext_offset[i];
}

int Flow::getChainNumber(int i)
{
	return offsets[i].first;
}

int Flow::getChainOffset(int i)
{
	return offsets[i].second;
}

void Flow::printFlow()
{
	cout << "===============FLOW DEBUGGING INFORMATION=======================" << endl;
	cout << "size of flow: " << fl.size() << endl;
	
	for (int i=0; i< fl.size(); i++)
	{
		cout << "Chain #" << i << endl;
		for (int j=0; j<fl[i].size(); j++)
		{
			cout << "[" << fl[i][j].offset << "]"<< endl;
		}
	}
	cout << "===============END OF FLOW DEBUGGING============================" << endl;
	return;
}

void Flow::print(string s)
{
	cout << s.c_str() << endl;
}
