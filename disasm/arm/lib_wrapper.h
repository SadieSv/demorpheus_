#ifndef _LIB_WRAPPER_H
#define _LIB_WRAPPER_H
#pragma once
#include <set>
#include <bitset>
#include <stdint.h>
#include <vector>
#include <iostream>

#define ARM_SIZE 4
#define THUMB_SIZE 2

#define ARM_MODE 0
#define THUMB_MODE 1

#define _BIG_ENDIAN    1
#define _LITTLE_ENDIAN 0

#include "armstorm/armstorm.h"
#include "libdisarm/disarm.h"

enum arm_type {
	BRANCH_LINK_EXCHANGE,
	BRANCH_EXCHANGE,
	BRANCH_LINK,
	BRANCH,
	
	LOAD,
	STORE,
	
	DATA_PROC,
	COPROCESSOR,
	DSP,
	
	SVC,
	
	_UNDEFINED,
	_INCORRECT
};

enum arm_data_instr_type {
	_AND,
	EOR,
	SUB,
	RSB,
	ADD,
	ADC,
	SBC,
	RSC,
	TST,
	TEQ,
	CMP,
	CMN,
	ORR,
	MOV,
	BIC,
	MVN,
	MAX,
	
	// for thumb
	ROR,
	ASR,
	LSL,
	LSR,
	NEG,
	MUL,
	OTHER
};

class instruct_risc
{
public:
	int size;
	arm_type type;
	da_group_t group;
	da_cond_t condition;
	arm_data_instr_type data_instr_type;
	
	int offset;
	int branch_offset;
	
	int reg_has_pc;

	bool change_pc;
	bool get_pc;
	bool change_flags;
	bool visible_branch_address;
	
	void printType();
	void printInstr();
	void printOffset();
	bool isARM;
	
	std::bitset<16> changed_regs;
	std::bitset<16> used_regs;
	char index_reg;
	
	int dest_reg;
	bool xor_dst;
	
	da_instr_t 		instr;
	da_instr_args_t args;
	const da_instr_t *_instr;
	const da_instr_args_t *_args;
	
	_DInst insts;
    _DecomposeInfo info;
	const _DecomposeInfo* _info;
	const _DInst* _insts;
	
	//if in chain is instruction with ext_off== true, that means reference to other chain
	// some kind of "chain merging"
	bool ext_off;
};

class instruct_arm :public instruct_risc
{
public:
        int size;
        //arm_type type;
		da_group_t group;
		da_cond_t condition;
		//arm_data_instr_type data_instr_type;
		
        int offset;
		int branch_offset;
	
		//std::bitset<16> changed_regs;
		//std::bitset<16> used_regs;
		
		bool change_pc;
		bool get_pc;
		bool change_flags;
		bool visible_branch_address;

		const da_instr_t *_instr;
		const da_instr_args_t *_args;
		
		void printInstr();/*
		void printType();
		void printInstrType();*/
		
	//if in chain is instruction with ext_off== true, that means reference to other chain
	// some kind of "chain merging"
	bool ext_off;
};

class instruct_thumb :public instruct_risc
{
public:
        int size;
        //arm_type type;
        int offset;
		int branch_offset;
		//arm_data_instr_type data_instr_type;
	
		//std::bitset<16> changed_regs;
		//std::bitset<16> used_regs;
		
		bool change_pc;
		bool get_pc;
		bool change_flags;
		bool visible_branch_address;
		
		const _DecomposeInfo* _info;
		const _DInst* _insts;
		
		void printInstr();/*
		void printType();
		void printInstrType();*/
		
	//if in chain is instruction with ext_off== true, that means reference to other chain
	// some kind of "chain merging"
	bool ext_off;
};

class Lib_wrapper
{
private:
	static std::bitset<16> get_arm_reglist(da_uint_t _reglist);
	static std::bitset<16> get_thumb_reglist(unsigned char reglist);
	
public:
	static /*instruct_arm*/instruct_risc 	arm_get_instr(da_word_t data, int big_endian);
	static /*instruct_thumb*/instruct_risc 	thumb_get_instr(const unsigned char* data, int big_endian, int operand_size);

};


#endif /* _LIB_WRAPPER_H */

