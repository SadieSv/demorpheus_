//
//  main.c
//  distorm_arm
//
//  Created by Gil Dabah on 12/7/12.
//  Copyright (c) 2012 Gil Dabah. All rights reserved.
//

// 1) Tests
// 2) Documentation

#include "armstorm.h"
#include <stdio.h>

int main(int argc, const char * argv[])
{
    unsigned char buf[] = {
	0x01, 0x10, 0x8F, 0xE2,
	0x11, 0xFF, 0x2F, 0xE1,

	0x02, 0x20, 0x01, 0x21,
	0x92, 0x1a, 0x0f, 0x02,
	0x19, 0x37, 0x01, 0xdf,
	0x06, 0x1c, 0x08, 0xa1,
	0x10, 0x22, 0x02, 0x37,
	0x01, 0xdf, 0x3f, 0x27,
	0x02, 0x21,

	0x30, 0x1c, 0x01, 0xdf,
	0x01, 0x39, 0xfb, 0xd5,
	0x05, 0xa0, 0x92, 0x1a,
	0x05, 0xb4, 0x69, 0x46,
	0x0b, 0x27,0x01, 0xdf,
	0xc0, 0x46};

    _DInst insts[100] = {0};
    _DecomposeInfo info = {0};
    info.address = 1;
    info.code = buf;
    info.codeLength = sizeof(buf);
    info.endianity = ENDIANITY_LITTLE;
    info.maxInstructions = 100;
    info.instructions = insts;
    armstorm_decompose(&info);
    for (int i = 0; i < info.decodedInstructionsCount; i++) {
        _TInst t = {0};
        armstorm_format(&info, &insts[i], &t);
        printf("%08llx %s %s\n", t.address, t.hex, t.instruction);
    }
    return 0;
}

