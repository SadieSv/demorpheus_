#include <cstdlib>
#include <iostream>
#include <cstring>
#include "Thumb.h"
#include "error.h"
#include "ARM.h"
/*
#include "../armstorm/armstorm.h"
#include "../libdisarm/disarm.h"*/
#include "../lib_wrapper.h"

using namespace std;

class Armulator{
public:
	Armulator();
	~Armulator();
	
	int execute( const unsigned char* buffer, int length, int mode, int i );
	void printRegs( bool arm_mode );

	int _length;
	ARM *arm;
	Thumb *thumb;
	
	CPU* cpu;
	
	int wxNum, unReadNum, unWriteNum;
	bool branchWrited;
	bool modeChanged;
	bool malicCall;
	
};







