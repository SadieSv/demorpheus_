/*! \file swi_semihost.cpp
	\brief The implementation of software interrupt handler
 */
#include "swi_semihost.h"
#include <iostream>
#include <time.h>
#include "error.h"
#include <fcntl.h>
#include <cstring>
#include <cstdlib>
#include <errno.h>

/*! \def O_BINARY
	\brief Define it when O_BINARY not defined
 */
#ifndef O_BINARY
#define O_BINARY 0
#endif

using namespace std;


/*! \var fopen_mode
	\brief 12 types of open file mode
 */
static int fopen_mode[]=
{
  O_RDONLY,		/* "r"   */
  O_RDONLY + O_BINARY,	/* "rb"  */
  O_RDWR,		/* "r+"  */
  O_RDWR + O_BINARY,		/* "r+b" */
  O_WRONLY + O_CREAT + O_TRUNC,	/* "w"   */
  O_WRONLY + O_BINARY + O_CREAT + O_TRUNC,	/* "wb"  */
  O_RDWR + O_CREAT + O_TRUNC,	/* "w+"  */
  O_RDWR + O_BINARY + O_CREAT + O_TRUNC,	/* "w+b" */
  O_WRONLY + O_APPEND + O_CREAT,	/* "a"   */
  O_WRONLY + O_BINARY + O_APPEND + O_CREAT,	/* "ab"  */
  O_RDWR + O_APPEND + O_CREAT,	/* "a+"  */
  O_RDWR + O_BINARY + O_APPEND + O_CREAT	/* "a+b" */
};

/*! \var previous_errno
	\brief Record of last operation error number, not to be used
 */
static int previous_errno = 0;

/**
  * Nothing to be done.
  */
 swi_semihost::swi_semihost()
{
    //fopen_mode[12][4]={"r","rb","r+","r+b","w","wb","w+","w+b","a","ab","a+","a+b"};
	
	prohibited_calls.push_back(2); // 0
	prohibited_calls.push_back(4); // 3 2-pc 3-pc
	prohibited_calls.push_back(5); // 3 1-pc
	prohibited_calls.push_back(6); // 0
	prohibited_calls.push_back(8); // 2 1-pc
	prohibited_calls.push_back(11); // 1 1-pc
	prohibited_calls.push_back(14); // 3 1-pc 3-pc
	prohibited_calls.push_back(15); // 2 1-pc 2-pc
	prohibited_calls.push_back(23); // 1 1-pc
	prohibited_calls.push_back(37); // 2
	prohibited_calls.push_back(41); // 
	prohibited_calls.push_back(63); // 2
	prohibited_calls.push_back(102); // 2
	prohibited_calls.push_back(281); // 
	prohibited_calls.push_back(282); // 
	prohibited_calls.push_back(283); // 
	prohibited_calls.push_back(284); // 
	prohibited_calls.push_back(285); // 
	/*
	system_call_table.push_back( "__NR_restart_syscall       " ); //   0
	system_call_table.push_back( "__NR_exit                  " ); //   1
	system_call_table.push_back( "__NR_fork                  " ); //   2
	system_call_table.push_back( "__NR_read                  " ); //   3
	system_call_table.push_back( "__NR_write                 " ); //   4
	system_call_table.push_back( "__NR_open                  " ); //   5
	system_call_table.push_back( "__NR_close                 " ); //   6
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_creat                 " ); //   8
	system_call_table.push_back( "__NR_link                  " ); //   9
	system_call_table.push_back( "__NR_unlink                " ); //  10
	system_call_table.push_back( "__NR_execve                " ); //  11
	system_call_table.push_back( "__NR_chdir                 " ); //  12
	system_call_table.push_back( "__NR_time                  " ); //  13
	system_call_table.push_back( "__NR_mknod                 " ); //  14
	system_call_table.push_back( "__NR_chmod                 " ); //  15
	system_call_table.push_back( "__NR_lchown                " ); //  16
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_lseek                 " ); //  19
	system_call_table.push_back( "__NR_getpid                " ); //  20
	system_call_table.push_back( "__NR_mount                 " ); //  21
	system_call_table.push_back( "__NR_umount                " ); //  22
	system_call_table.push_back( "__NR_setuid                " ); //  23
	system_call_table.push_back( "__NR_getuid                " ); //  24
	system_call_table.push_back( "__NR_stime                 " ); //  25
	system_call_table.push_back( "__NR_ptrace                " ); //  26
	system_call_table.push_back( "__NR_alarm                 " ); //  27
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_pause                 " ); //  29
	system_call_table.push_back( "__NR_utime                 " ); //  30
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_access                " ); //  33
	system_call_table.push_back( "__NR_nice                  " ); //  34
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_sync                  " ); //  36
	system_call_table.push_back( "__NR_kill                  " ); //  37
	system_call_table.push_back( "__NR_rename                " ); //  38
	system_call_table.push_back( "__NR_mkdir                 " ); //  39
	system_call_table.push_back( "__NR_rmdir                 " ); //  40
	system_call_table.push_back( "__NR_dup                   " ); //  41
	system_call_table.push_back( "__NR_pipe                  " ); //  42
	system_call_table.push_back( "__NR_times                 " ); //  43
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_brk                   " ); //  45
	system_call_table.push_back( "__NR_setgid                " ); //  46
	system_call_table.push_back( "__NR_getgid                " ); //  47
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_geteuid               " ); //  49
	system_call_table.push_back( "__NR_getegid               " ); //  50
	system_call_table.push_back( "__NR_acct                  " ); //  51
	system_call_table.push_back( "__NR_umount2               " ); //  52
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_ioctl                 " ); //  54
	system_call_table.push_back( "__NR_fcntl                 " ); //  55
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_setpgid               " ); //  57
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_umask                 " ); //  60
	system_call_table.push_back( "__NR_chroot                " ); //  61
	system_call_table.push_back( "__NR_ustat                 " ); //  62
	system_call_table.push_back( "__NR_dup2                  " ); //  63
	system_call_table.push_back( "__NR_getppid               " ); //  64
	system_call_table.push_back( "__NR_getpgrp               " ); //  65
	system_call_table.push_back( "__NR_setsid                " ); //  66
	system_call_table.push_back( "__NR_sigaction             " ); //  67
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_setreuid              " ); //  70
	system_call_table.push_back( "__NR_setregid              " ); //  71
	system_call_table.push_back( "__NR_sigsuspend            " ); //  72
	system_call_table.push_back( "__NR_sigpending            " ); //  73
	system_call_table.push_back( "__NR_sethostname           " ); //  74
	system_call_table.push_back( "__NR_setrlimit             " ); //  75
	system_call_table.push_back( "__NR_getrlimit             " ); //  76
	system_call_table.push_back( "__NR_getrusage             " ); //  77
	system_call_table.push_back( "__NR_gettimeofday          " ); //  78
	system_call_table.push_back( "__NR_settimeofday          " ); //  79
	system_call_table.push_back( "__NR_getgroups             " ); //  80
	system_call_table.push_back( "__NR_setgroups             " ); //  81
	system_call_table.push_back( "__NR_select                " ); //  82
	system_call_table.push_back( "__NR_symlink               " ); //  83
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_readlink              " ); //  85
	system_call_table.push_back( "__NR_uselib                " ); //  86
	system_call_table.push_back( "__NR_swapon                " ); //  87
	system_call_table.push_back( "__NR_reboot                " ); //  88
	system_call_table.push_back( "__NR_readdir               " ); //  89
	system_call_table.push_back( "__NR_mmap                  " ); //  90
	system_call_table.push_back( "__NR_munmap                " ); //  91
	system_call_table.push_back( "__NR_truncate              " ); //  92
	system_call_table.push_back( "__NR_ftruncate             " ); //  93
	system_call_table.push_back( "__NR_fchmod                " ); //  94
	system_call_table.push_back( "__NR_fchown                " ); //  95
	system_call_table.push_back( "__NR_getpriority           " ); //  96
	system_call_table.push_back( "__NR_setpriority           " ); //  97
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_statfs                " ); //  99
	system_call_table.push_back( "__NR_fstatfs               " ); // 100
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_socketcall            " ); // 102
	system_call_table.push_back( "__NR_syslog                " ); // 103
	system_call_table.push_back( "__NR_setitimer             " ); // 104
	system_call_table.push_back( "__NR_getitimer             " ); // 105
	system_call_table.push_back( "__NR_stat                  " ); // 106
	system_call_table.push_back( "__NR_lstat                 " ); // 107
	system_call_table.push_back( "__NR_fstat                 " ); // 108
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_vhangup               " ); // 111
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_syscall               " ); // 113
	system_call_table.push_back( "__NR_wait4                 " ); // 114
	system_call_table.push_back( "__NR_swapoff               " ); // 115
	system_call_table.push_back( "__NR_sysinfo               " ); // 116
	system_call_table.push_back( "__NR_ipc                   " ); // 117
	system_call_table.push_back( "__NR_fsync                 " ); // 118
	system_call_table.push_back( "__NR_sigreturn             " ); // 119
	system_call_table.push_back( "__NR_clone                 " ); // 120
	system_call_table.push_back( "__NR_setdomainname         " ); // 121
	system_call_table.push_back( "__NR_uname                 " ); // 122
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_adjtimex              " ); // 124
	system_call_table.push_back( "__NR_mprotect              " ); // 125
	system_call_table.push_back( "__NR_sigprocmask           " ); // 126
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_init_module           " ); // 128
	system_call_table.push_back( "__NR_delete_module         " ); // 129
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_quotactl              " ); // 131
	system_call_table.push_back( "__NR_getpgid               " ); // 132
	system_call_table.push_back( "__NR_fchdir                " ); // 133
	system_call_table.push_back( "__NR_bdflush               " ); // 134
	system_call_table.push_back( "__NR_sysfs                 " ); // 135
	system_call_table.push_back( "__NR_personality           " ); // 136
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_setfsuid              " ); // 138
	system_call_table.push_back( "__NR_setfsgid              " ); // 139
	system_call_table.push_back( "__NR__llseek               " ); // 140
	system_call_table.push_back( "__NR_getdents              " ); // 141
	system_call_table.push_back( "__NR__newselect            " ); // 142
	system_call_table.push_back( "__NR_flock                 " ); // 143
	system_call_table.push_back( "__NR_msync                 " ); // 144
	system_call_table.push_back( "__NR_readv                 " ); // 145
	system_call_table.push_back( "__NR_writev                " ); // 146
	system_call_table.push_back( "__NR_getsid                " ); // 147
	system_call_table.push_back( "__NR_fdatasync             " ); // 148
	system_call_table.push_back( "__NR__sysctl               " ); // 149
	system_call_table.push_back( "__NR_mlock                 " ); // 150
	system_call_table.push_back( "__NR_munlock               " ); // 151
	system_call_table.push_back( "__NR_mlockall              " ); // 152
	system_call_table.push_back( "__NR_munlockall            " ); // 153
	system_call_table.push_back( "__NR_sched_setparam        " ); // 154
	system_call_table.push_back( "__NR_sched_getparam        " ); // 155
	system_call_table.push_back( "__NR_sched_setscheduler    " ); // 156
	system_call_table.push_back( "__NR_sched_getscheduler    " ); // 157
	system_call_table.push_back( "__NR_sched_yield           " ); // 158
	system_call_table.push_back( "__NR_sched_get_priority_max" ); // 159
	system_call_table.push_back( "__NR_sched_get_priority_min" ); // 160
	system_call_table.push_back( "__NR_sched_rr_get_interval " ); // 161
	system_call_table.push_back( "__NR_nanosleep             " ); // 162
	system_call_table.push_back( "__NR_mremap                " ); // 163
	system_call_table.push_back( "__NR_setresuid             " ); // 164
	system_call_table.push_back( "__NR_getresuid             " ); // 165
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_poll                  " ); // 168
	system_call_table.push_back( "__NR_nfsservctl            " ); // 169
	system_call_table.push_back( "__NR_setresgid             " ); // 170
	system_call_table.push_back( "__NR_getresgid             " ); // 171
	system_call_table.push_back( "__NR_prctl                 " ); // 172
	system_call_table.push_back( "__NR_rt_sigreturn          " ); // 173
	system_call_table.push_back( "__NR_rt_sigaction          " ); // 174
	system_call_table.push_back( "__NR_rt_sigprocmask        " ); // 175
	system_call_table.push_back( "__NR_rt_sigpending         " ); // 176
	system_call_table.push_back( "__NR_rt_sigtimedwait       " ); // 177
	system_call_table.push_back( "__NR_rt_sigqueueinfo       " ); // 178
	system_call_table.push_back( "__NR_rt_sigsuspend         " ); // 179
	system_call_table.push_back( "__NR_pread64               " ); // 180
	system_call_table.push_back( "__NR_pwrite64              " ); // 181
	system_call_table.push_back( "__NR_chown                 " ); // 182
	system_call_table.push_back( "__NR_getcwd                " ); // 183
	system_call_table.push_back( "__NR_capget                " ); // 184
	system_call_table.push_back( "__NR_capset                " ); // 185
	system_call_table.push_back( "__NR_sigaltstack           " ); // 186
	system_call_table.push_back( "__NR_sendfile              " ); // 187
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_vfork                 " ); // 190
	system_call_table.push_back( "__NR_ugetrlimit            " ); // 191
	system_call_table.push_back( "__NR_mmap2                 " ); // 192
	system_call_table.push_back( "__NR_truncate64            " ); // 193
	system_call_table.push_back( "__NR_ftruncate64           " ); // 194
	system_call_table.push_back( "__NR_stat64                " ); // 195
	system_call_table.push_back( "__NR_lstat64               " ); // 196
	system_call_table.push_back( "__NR_fstat64               " ); // 197
	system_call_table.push_back( "__NR_lchown32              " ); // 198
	system_call_table.push_back( "__NR_getuid32              " ); // 199
	system_call_table.push_back( "__NR_getgid32              " ); // 200
	system_call_table.push_back( "__NR_geteuid32             " ); // 201
	system_call_table.push_back( "__NR_getegid32             " ); // 202
	system_call_table.push_back( "__NR_setreuid32            " ); // 203
	system_call_table.push_back( "__NR_setregid32            " ); // 204
	system_call_table.push_back( "__NR_getgroups32           " ); // 205
	system_call_table.push_back( "__NR_setgroups32           " ); // 206
	system_call_table.push_back( "__NR_fchown32              " ); // 207
	system_call_table.push_back( "__NR_setresuid32           " ); // 208
	system_call_table.push_back( "__NR_getresuid32           " ); // 209
	system_call_table.push_back( "__NR_setresgid32           " ); // 210
	system_call_table.push_back( "__NR_getresgid32           " ); // 211
	system_call_table.push_back( "__NR_chown32               " ); // 212
	system_call_table.push_back( "__NR_setuid32              " ); // 213
	system_call_table.push_back( "__NR_setgid32              " ); // 214
	system_call_table.push_back( "__NR_setfsuid32            " ); // 215
	system_call_table.push_back( "__NR_setfsgid32            " ); // 216
	system_call_table.push_back( "__NR_getdents64            " ); // 217
	system_call_table.push_back( "__NR_pivot_root            " ); // 218
	system_call_table.push_back( "__NR_mincore               " ); // 219
	system_call_table.push_back( "__NR_madvise               " ); // 220
	system_call_table.push_back( "__NR_fcntl64               " ); // 221
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_gettid                " ); // 224
	system_call_table.push_back( "__NR_readahead             " ); // 225
	system_call_table.push_back( "__NR_setxattr              " ); // 226
	system_call_table.push_back( "__NR_lsetxattr             " ); // 227
	system_call_table.push_back( "__NR_fsetxattr             " ); // 228
	system_call_table.push_back( "__NR_getxattr              " ); // 229
	system_call_table.push_back( "__NR_lgetxattr             " ); // 230
	system_call_table.push_back( "__NR_fgetxattr             " ); // 231
	system_call_table.push_back( "__NR_listxattr             " ); // 232
	system_call_table.push_back( "__NR_llistxattr            " ); // 233
	system_call_table.push_back( "__NR_flistxattr            " ); // 234
	system_call_table.push_back( "__NR_removexattr           " ); // 235
	system_call_table.push_back( "__NR_lremovexattr          " ); // 236
	system_call_table.push_back( "__NR_fremovexattr          " ); // 237
	system_call_table.push_back( "__NR_tkill                 " ); // 238
	system_call_table.push_back( "__NR_sendfile64            " ); // 239
	system_call_table.push_back( "__NR_futex                 " ); // 240
	system_call_table.push_back( "__NR_sched_setaffinity     " ); // 241
	system_call_table.push_back( "__NR_sched_getaffinity     " ); // 242
	system_call_table.push_back( "__NR_io_setup              " ); // 243
	system_call_table.push_back( "__NR_io_destroy            " ); // 244
	system_call_table.push_back( "__NR_io_getevents          " ); // 245
	system_call_table.push_back( "__NR_io_submit             " ); // 246
	system_call_table.push_back( "__NR_io_cancel             " ); // 247
	system_call_table.push_back( "__NR_exit_group            " ); // 248
	system_call_table.push_back( "__NR_lookup_dcookie        " ); // 249
	system_call_table.push_back( "__NR_epoll_create          " ); // 250
	system_call_table.push_back( "__NR_epoll_ctl             " ); // 251
	system_call_table.push_back( "__NR_epoll_wait            " ); // 252
	system_call_table.push_back( "__NR_remap_file_pages      " ); // 253
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__UNDEFINED" ); // //===
	system_call_table.push_back( "__NR_set_tid_address       " ); // 256
	system_call_table.push_back( "__NR_timer_create          " ); // 257
	system_call_table.push_back( "__NR_timer_settime         " ); // 258
	system_call_table.push_back( "__NR_timer_gettime         " ); // 259
	system_call_table.push_back( "__NR_timer_getoverrun      " ); // 260
	system_call_table.push_back( "__NR_timer_delete          " ); // 261
	system_call_table.push_back( "__NR_clock_settime         " ); // 262
	system_call_table.push_back( "__NR_clock_gettime         " ); // 263
	system_call_table.push_back( "__NR_clock_getres          " ); // 264
	system_call_table.push_back( "__NR_clock_nanosleep       " ); // 265
	system_call_table.push_back( "__NR_statfs64              " ); // 266
	system_call_table.push_back( "__NR_fstatfs64             " ); // 267
	system_call_table.push_back( "__NR_tgkill                " ); // 268
	system_call_table.push_back( "__NR_utimes                " ); // 269
	system_call_table.push_back( "__NR_arm_fadvise64_64      " ); // 270
	system_call_table.push_back( "__NR_pciconfig_iobase      " ); // 271
	system_call_table.push_back( "__NR_pciconfig_read        " ); // 272
	system_call_table.push_back( "__NR_pciconfig_write       " ); // 273
	system_call_table.push_back( "__NR_mq_open               " ); // 274
	system_call_table.push_back( "__NR_mq_unlink             " ); // 275
	system_call_table.push_back( "__NR_mq_timedsend          " ); // 276
	system_call_table.push_back( "__NR_mq_timedreceive       " ); // 277
	system_call_table.push_back( "__NR_mq_notify             " ); // 278
	system_call_table.push_back( "__NR_mq_getsetattr         " ); // 279
	system_call_table.push_back( "__NR_waitid                " ); // 280
	system_call_table.push_back( "__NR_socket                " ); // 281
	system_call_table.push_back( "__NR_bind                  " ); // 282
	system_call_table.push_back( "__NR_connect               " ); // 283
	system_call_table.push_back( "__NR_listen                " ); // 284
	system_call_table.push_back( "__NR_accept                " ); // 285
	system_call_table.push_back( "__NR_getsockname           " ); // 286
	system_call_table.push_back( "__NR_getpeername           " ); // 287
	system_call_table.push_back( "__NR_socketpair            " ); // 288
	system_call_table.push_back( "__NR_send                  " ); // 289
	system_call_table.push_back( "__NR_sendto                " ); // 290
	system_call_table.push_back( "__NR_recv                  " ); // 291
	system_call_table.push_back( "__NR_recvfrom              " ); // 292
	system_call_table.push_back( "__NR_shutdown              " ); // 293
	system_call_table.push_back( "__NR_setsockopt            " ); // 294
	system_call_table.push_back( "__NR_getsockopt            " ); // 295
	system_call_table.push_back( "__NR_sendmsg               " ); // 296
	system_call_table.push_back( "__NR_recvmsg               " ); // 297
	system_call_table.push_back( "__NR_semop                 " ); // 298
	system_call_table.push_back( "__NR_semget                " ); // 299
	system_call_table.push_back( "__NR_semctl                " ); // 300
	system_call_table.push_back( "__NR_msgsnd                " ); // 301
	system_call_table.push_back( "__NR_msgrcv                " ); // 302
	system_call_table.push_back( "__NR_msgget                " ); // 303
	system_call_table.push_back( "__NR_msgctl                " ); // 304
	system_call_table.push_back( "__NR_shmat                 " ); // 305
	system_call_table.push_back( "__NR_shmdt                 " ); // 306
	system_call_table.push_back( "__NR_shmget                " ); // 307
	system_call_table.push_back( "__NR_shmctl                " ); // 308
	system_call_table.push_back( "__NR_add_key               " ); // 309
	system_call_table.push_back( "__NR_request_key           " ); // 310
	system_call_table.push_back( "__NR_keyctl                " ); // 311
	system_call_table.push_back( "__NR_semtimedop            " ); // 312
	system_call_table.push_back( "__NR_vserver               " ); // 313
	system_call_table.push_back( "__NR_ioprio_set            " ); // 314
	system_call_table.push_back( "__NR_ioprio_get            " ); // 315
	system_call_table.push_back( "__NR_inotify_init          " ); // 316
	system_call_table.push_back( "__NR_inotify_add_watch     " ); // 317
	system_call_table.push_back( "__NR_inotify_rm_watch      " ); // 318
	system_call_table.push_back( "__NR_mbind                 " ); // 319
	system_call_table.push_back( "__NR_get_mempolicy         " ); // 320
	system_call_table.push_back( "__NR_set_mempolicy         " ); // 321
	system_call_table.push_back( "__NR_openat                " ); // 322
	system_call_table.push_back( "__NR_mkdirat               " ); // 323
	system_call_table.push_back( "__NR_mknodat               " ); // 324
	system_call_table.push_back( "__NR_fchownat              " ); // 325
	system_call_table.push_back( "__NR_futimesat             " ); // 326
	system_call_table.push_back( "__NR_fstatat64             " ); // 327
	system_call_table.push_back( "__NR_unlinkat              " ); // 328
	system_call_table.push_back( "__NR_renameat              " ); // 329
	system_call_table.push_back( "__NR_linkat                " ); // 330
	system_call_table.push_back( "__NR_symlinkat             " ); // 331
	system_call_table.push_back( "__NR_readlinkat            " ); // 332
	system_call_table.push_back( "__NR_fchmodat              " ); // 333
	system_call_table.push_back( "__NR_faccessat             " ); // 334
	system_call_table.push_back( "__NR_pselect6              " ); // 335
	system_call_table.push_back( "__NR_ppoll                 " ); // 336
	system_call_table.push_back( "__NR_unshare               " ); // 337
	system_call_table.push_back( "__NR_set_robust_list       " ); // 338
	system_call_table.push_back( "__NR_get_robust_list       " ); // 339
	system_call_table.push_back( "__NR_splice                " ); // 340
	system_call_table.push_back( "__NR_arm_sync_file_range   " ); // 341
	//system_call_table.push_back( "__NR_sync_file_range2      " ); // //===
	system_call_table.push_back( "__NR_tee                   " ); // 342
	system_call_table.push_back( "__NR_vmsplice              " ); // 343
	system_call_table.push_back( "__NR_move_pages            " ); // 344
	system_call_table.push_back( "__NR_getcpu                " ); // 345
	system_call_table.push_back( "__NR_epoll_pwait           " ); // 346
	system_call_table.push_back( "__NR_kexec_load            " ); // 347
	system_call_table.push_back( "__NR_utimensat             " ); // 348
	system_call_table.push_back( "__NR_signalfd              " ); // 349
	system_call_table.push_back( "__NR_timerfd_create        " ); // 350
	system_call_table.push_back( "__NR_eventfd               " ); // 351
	system_call_table.push_back( "__NR_fallocate             " ); // 352
	system_call_table.push_back( "__NR_timerfd_settime       " ); // 353
	system_call_table.push_back( "__NR_timerfd_gettime       " ); // 354
	system_call_table.push_back( "__NR_signalfd4             " ); // 355
	system_call_table.push_back( "__NR_eventfd2              " ); // 356
	system_call_table.push_back( "__NR_epoll_create1         " ); // 357
	system_call_table.push_back( "__NR_dup3                  " ); // 358
	system_call_table.push_back( "__NR_pipe2                 " ); // 359
	system_call_table.push_back( "__NR_inotify_init1         " ); // 360
	system_call_table.push_back( "__NR_preadv                " ); // 361
	system_call_table.push_back( "__NR_pwritev               " ); // 362
	system_call_table.push_back( "__NR_rt_tgsigqueueinfo     " ); // 363
	system_call_table.push_back( "__NR_perf_event_open       " ); // 364
	*/
}

/**
  * Nothing to be done.
  */
 swi_semihost::~swi_semihost()
{

}

/**
  * Get the heap starting address, heap size, stact high address, stack size information
  * @param heap_addr The heap starting address
  * @param heap_limit The heap size
  * @param stack_addr The stack high address
  * @param stack_limit The stack size
  */
void swi_semihost::getHeapInfo(int heap_addr, int heap_limit, int stack_addr, int stack_limit)
{
    hs_addr = heap_addr;
    hs_sz = heap_limit;
    ss_addr = stack_addr;
    ss_sz = stack_limit;
}

/**
  * Get the MMU modular pointer, in order to get parameter from r1
  * @param mmu The pointer to MMU modular
  */
void swi_semihost::getMMU(MMU *mmu)
{
    my_mmu = mmu;
}


void swi_semihost::printSWI(int swi_type)
{
	cout<< system_call_table[swi_type] ;
}

/**
  * Get the pointer to parameter array, namely the general purpose registers
  * @param para The pointer to parameter array
  */
void swi_semihost::get_para(const GP_Reg *para)
{
    swi_type = para[7];
	//swi_type_old = para[0];
    parameter = const_cast<GP_Reg *>(para);
}

/**
  * Choose right swi handler according to the swi_type
  */
void swi_semihost::swi_handler()
{
	/*
	if ( (swi_type <= 364) && (swi_type > -1) )
	{
		printSWI(swi_type); cout<<" ="<<swi_type;
	}
	else
		cout << "__OVER_UNDEF";
	
	cout<< " || " ;
	
	if ( (swi_type_old <= 364) && (swi_type_old > -1) )
	{
		printSWI(swi_type_old); cout<<" ="<<swi_type_old;
	}
	else
		cout << "__OVER_UNDEF";
		
	cout<<endl;
	*/
	
	
	
    /*switch (swi_type)
    {
    	case SYS_OPEN:
            sys_open();
    		break;
        case SYS_CLOSE:
            sys_close();
            break;
        case SYS_WRITEC:
            sys_writec();
            break;
        case SYS_WRITE0:
            sys_write0();
        	break;
        case SYS_WRITE:
            sys_write();
        	break;
        case SYS_READ:
            sys_read();
        	break;
        case SYS_READC:
            sys_readc();
        	break;
        case SYS_ISERROR:
            sys_iserror();
        	break;
        case SYS_ISTTY:
            sys_istty();
        	break;
        case SYS_SEEK:
            sys_seek();
        	break;
        case SYS_FLEN:
            sys_flen();
        	break;
        case SYS_TMPNAM:
            sys_tmpnam();
        	break;
        case SYS_REMOVE:
            sys_remove();
        	break;
        case SYS_RENAME:
            sys_rename();
        	break;
        case SYS_CLOCK:
            sys_clock();
        	break;
        case SYS_TIME:
            sys_time();
        	break;
        case SYS_SYSTEM:
            sys_system();
        	break;
        case SYS_ERRNO:
            sys_errno();
        	break;
        case SYS_GET_CMDLINE:
            sys_get_cmdline();
        	break;
        case SYS_HEAPINFO:
            sys_heapinfo();
        	break;
        case SYS_KILL:
            sys_kill();
            break;
        case SYS_ELAPSED:
            sys_elapsed();
        	break;
        case SYS_TICKFREQ:
            sys_tickfreq();
        	break;
    	default:
    		break;
    }*/

}

/**
  *
  */
void swi_semihost::sys_kill()
{
    ProgramEnd e;
    throw e;
}

/**
  *
  */
void swi_semihost::sys_elapsed()
{

}

/**
  *
  */
void swi_semihost::sys_heapinfo()
{
    int address;

    address = my_mmu->get_word(parameter[1]);

    my_mmu->set_word(address, hs_addr);
    my_mmu->set_word(address + 4, hs_sz);
    my_mmu->set_word(address + 8, ss_addr);
    my_mmu->set_word(address + 12, ss_sz);
}

/**
  *
  */
void swi_semihost::sys_get_cmdline()
{
    char cmdline[arg_len + 1];
    int cmd_pointer = my_mmu->get_word(parameter[1]);

    memset(cmdline, 0, arg_len + 1);
    memcpy(cmdline, argv, arg_len);

    for (int i = 0; i < strlen(cmdline); i++)
        my_mmu->set_byte(cmd_pointer + i, cmdline[i]);

    my_mmu->set_byte(cmd_pointer + strlen(cmdline), 0);

    //UnexpectInst e;
    //e.error_name = "cmd";
    //throw e;

    parameter[0] = 0;
}

/**
  *
  */
void swi_semihost::sys_errno()
{
    parameter[0] = previous_errno;
}

/**
  *
  */
void swi_semihost::sys_system()
{
    int cmd_pointer = my_mmu->get_word(parameter[1]);
    int len = my_mmu->get_word(parameter[1] + 4);
    char cmd[len + 1];

    for (int i = 0; i < len; i++)
        cmd[i] = my_mmu->get_byte(cmd_pointer + i);
    cmd[len] = 0;

    parameter[0] = system(cmd);
}

/**
  *
  */
void swi_semihost::sys_time()
{
    parameter[0] = time(NULL);
    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_clock()
{
    parameter[0] =
#ifdef CLOCKS_PER_SEC
    (CLOCKS_PER_SEC >= 100)
    ? (clock () / (CLOCKS_PER_SEC / 100))
    : ((clock () * 100) / CLOCKS_PER_SEC);
#else
    /* Presume unix... clock() returns microseconds.  */
    (clock () / 10000);
#endif

    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_rename()
{
    int file_pre = my_mmu->get_word(parameter[1]);
    int pre_len = my_mmu->get_word(parameter[1] + 4);
    int file_re = my_mmu->get_word(parameter[1] + 8);
    int re_len = my_mmu->get_word(parameter[1] + 12);

    char pre_name[pre_len + 1];
    char re_name[re_len + 1];

    for (int i = 0; i < pre_len; i++)
        pre_name[i] = my_mmu->get_byte(file_pre + i);
    pre_name[pre_len] = 0;

    for (int i = 0; i < re_len; i++)
        re_name[i] = my_mmu->get_byte(file_re + i);
    re_name[re_len] = 0;

    parameter[0] = rename(pre_name, re_name);
}

/**
  *
  */
void swi_semihost::sys_remove()
{
    int file_pointer = my_mmu->get_word(parameter[1]);
    int length = my_mmu->get_word(parameter[1] + 4);

    char *file_name = new char[length + 1];

    for (int i = 0; i < length; i++)
        file_name[i] = my_mmu->get_byte(file_pointer + i);
    file_name[length] = 0;

    parameter[0] = remove(file_name);
}

/**
  *
  */
void swi_semihost::sys_tmpnam()
{
//    int buffer_pointer = my_mmu->get_word(parameter[1]);
//    FILE *afile = reinterpret_cast<FILE *>(my_mmu->get_word(parameter[1] + 4));
//    int len = my_mmu->get_word(parameter[1] + 8);
//    char buffer[len];

    //strncpy(buffer, afile->_tmpfname, strlen(afile->_tmpfname));

    //for (int i = 0; i < strlen(afile->_tmpfname); i++)
    //    my_mmu->set_byte(buffer_pointer + i, buffer[i]);

    parameter[0] = 0;

}

/**
  *
  */
void swi_semihost::sys_flen()
{
    int handler = my_mmu->get_word(parameter[1]);

    if (handler == 0 || handler  > 64)
    {
        //errno
        parameter[0] = -1;
        return;
    }

    int addr = lseek(handler, 0, 1);//SEEK_CUR
    parameter[0] = lseek(handler, 0, 2);//SEEK_END
    lseek(handler, addr, 0);//SEEK_SET

    //get_errno();
    previous_errno = errno;

}

/**
  *
  */
void swi_semihost::sys_seek()
{
    int handler = my_mmu->get_word(parameter[1]);
    long pos = my_mmu->get_word(parameter[1] + 4);

    parameter[0] = -1 >= lseek(handler, pos, 0);//SEEK_SET

    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_istty()
{
    ARMUL_WORD isatty =  my_mmu->get_word(parameter[1]);

    if (isatty > 2 && isatty <= 64)
    {
        parameter[0] = 0;
    }
    else
    {
        parameter[0] = isatty;
    }
    //parameter[0] = -1;
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_iserror()
{
    parameter[0] = 0;
    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_readc()
{
    char msg;
    std::cin>>msg;

    parameter[0] = msg;
}

/**
  *
  */
void swi_semihost::sys_read()
{
    int handler = my_mmu->get_word(parameter[1]);
    int file_pointer = my_mmu->get_word(parameter[1] +4);
    uint32_t len = my_mmu->get_word(parameter[1] + 8);

    char *data = new char[len+1];

    int res = read(handler, data, len);

    if (res > 0)
        for (int i = 0; i < res; i++)
        {
            my_mmu->set_byte(file_pointer + i, data[i]);
        }

    parameter[0] = res == -1 ? -1 : len - res;

    delete []data;
    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_write()
{
    int handler = my_mmu->get_word(parameter[1]);
    int file_pointer = my_mmu->get_word(parameter[1] +4);
    uint32_t len = my_mmu->get_word(parameter[1] + 8);

    char *data = new char[len+1];

    for (int i = 0; i < len; i++)
        data[i] = my_mmu->get_byte(file_pointer + i);

    data[len] = 0;

    int res = write(handler, data, len);

    parameter[0] = res == -1 ? -1 : len - res;

    delete []data;

    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_write0()
{
    char msg;
    int i = 0;
    while ((msg = my_mmu->get_byte(parameter[1] + i)) != 0)
        std::cout<<msg;
}

/**
  *
  */
void swi_semihost::sys_writec()
{
    char msg = my_mmu->get_byte(parameter[1]);
    std::cout<<msg;
}

/**
  *
  */
void swi_semihost::sys_close()
{
    int handler = my_mmu->get_word(parameter[1]);

    if (handler != 1)
        close(handler);
    //get_errno();
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_open()
{
    int file_pointer = my_mmu->get_word(parameter[1]);
    int mode = my_mmu->get_word(parameter[1] + 4);
    int length = my_mmu->get_word(parameter[1] + 8);

    char *file_name = new char[length + 1];

    for (int i = 0; i < length; i++)
        file_name[i] = my_mmu->get_byte(file_pointer + i);
    file_name[length] = 0;

    if (strcmp(file_name, ":tt") == 0)
    {
        if (mode >= 0 && mode <= 3)
            parameter[0] = 0;//stdin
        else
            parameter[0] = 1;//stdout
    }
    else
    {
        parameter[0] = open(file_name,fopen_mode[mode]);
        //get_errno();
    }

    delete []file_name;
    previous_errno = errno;
}

/**
  *
  */
void swi_semihost::sys_tickfreq()
{

}

void swi_semihost::getArg(char *arg, int len)
{
    memset(argv, 0, 100);
    
    for (int i = 0; i < len; i++)
        argv[i] = arg[i];

    arg_len = len;
}

