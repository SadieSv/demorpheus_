
#include "lib_wrapper.h"

using namespace std;

std::bitset<16> Lib_wrapper::get_arm_reglist(da_uint_t _reglist)
{
	std::bitset<16> regs;
	regs.reset();
	da_uint_t reglist = _reglist;
	int comma = 0;
	int range_start = -1;
	int i = 0;
	for (i = 0; reglist; i++) {
		if (reglist & 1 && range_start == -1) {
			range_start = i;
		}

		reglist >>= 1;

		if (!(reglist & 1)) {
			if (range_start == i) {
				//if (comma) //fprintf(f, ",");
				//fprintf(f, " r%d", i);
				regs[i] = 1;
				comma = 1;
			} else if (i > 0 && range_start == i-1) {
				//if (comma) //fprintf(f, ",");
				//fprintf(f, " r%d, r%d", range_start, i);
				regs[range_start] = 1;
				regs[i] = 1;
				comma = 1;
			} else if (range_start >= 0) {
				//if (comma) //fprintf(f, ",");
				//fprintf(f, " r%d-r%d", range_start, i);
				for (int k=range_start; k<=i; k++)
					regs[k] = 1;
				comma = 1;
			}
			range_start = -1;
		}
	}
	return regs;
}

std::bitset<16> Lib_wrapper::get_thumb_reglist(unsigned char reglist)
{
	unsigned char mask = reglist;//op->value;
	std::bitset<16> regs;
	regs.reset();
	
	int backup = mask;
    int base = 0;
    int runLength = 0;
	
	if ( mask == 0 ) return regs;
    while (!(mask & 1)) base++, mask >>= 1;
    while (mask & 1) runLength++, mask >>= 1;
    if (!mask && runLength > 2) {
        char buf[20];
        //sprintf(buf, "R%d-R%d", base, base + runLength - 1);
        //strcat(str, buf);
		for ( int k=base; k<=base + runLength - 1; k++ )
			//regs[ _REGISTERS[k] ] = 1;
			regs[ k ] = 1;
    } else {
        int prev = 0;
        mask = backup;
        for (int j = 0; mask; j++) {
            if (mask & (1 << j)) {
                //if (prev) //strcat(str, ", ");
                //strcat(str, (const char*)_REGISTERS[j]);
				//regs[ _REGISTERS[j] ] = 1;
				regs[ j ] = 1;
                mask ^= (1 << j);
                prev = 1;
            }
        }
    }
	return regs;
}

/*instruct_arm*/instruct_risc Lib_wrapper::arm_get_instr(da_word_t data, int big_endian)
{
	/*instruct_arm*/instruct_risc ins;
	//da_word_t 		data;
    da_instr_t 		instr;
	da_instr_args_t args;
	
	da_instr_parse(&instr, data, big_endian);
	da_instr_parse_args(&args, &instr);
	
	/*TEST*/
	/*
	da_instr_fprint(stdout, &instr, &args, 0);
	//printf("\n");
	*/
	ins.instr = instr;
	ins.args = args;
	//ins._instr = &(ins.instr);
	//ins._args = &(ins.args);
	/*TEST*/
	
	ins.group = instr.group;
	ins.size = ARM_SIZE;
	ins.visible_branch_address = false;
	ins.reg_has_pc = -1;
	ins.isARM = true;
	
	ins.branch_offset = 0;
	ins.change_flags = false;
	ins.dest_reg = -1;
	ins.xor_dst = false;
	
	// ====================== START OF SWITCH ============================
	switch (instr.group) {
	case DA_GROUP_BKPT:
		break;
	// ------------------------- BRANCH -----------------------------
	case DA_GROUP_BL:
		if (args.bl.link)
		{
			ins.type = BRANCH_LINK;
			ins.changed_regs[ 14 ] = 1;
		}
		else
		{
			ins.type = BRANCH;
		}
		ins.branch_offset = da_instr_branch_target(args.bl.off,0);
		ins.visible_branch_address = true;
		ins.condition = args.bl.cond;
		break;
		
	case DA_GROUP_BLX_IMM:
		ins.type = BRANCH_LINK_EXCHANGE;
		ins.condition = DA_COND_AL;
		ins.branch_offset = da_instr_branch_target(args.blx_imm.off,0);
		ins.visible_branch_address = true;
		ins.changed_regs[ 14 ] = 1;
		break;
		
	case DA_GROUP_BLX_REG:
		if ( args.blx_reg.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		
		if ( args.blx_reg.link )
		{
			ins.type = BRANCH_LINK_EXCHANGE;
			ins.changed_regs[ 14 ] = 1;
			ins.used_regs[ args.blx_reg.rm ] = 1;
		}
		else
		{
			ins.type = BRANCH_EXCHANGE;
			ins.used_regs[ args.blx_reg.rm ] = 1;
		}

		ins.condition = args.blx_reg.cond;
		break;
		
	// ------------------------- CLZ (DATA_PROC) -----------------------------
	case DA_GROUP_CLZ:
		if ( args.clz.rd == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.type = DATA_PROC;
		ins.condition = args.clz.cond;
		ins.changed_regs[ args.clz.rd ] = 1;
		ins.used_regs[ args.clz.rm ] = 1;
		break;
		
	// ------------------------- COPROCESSOR -----------------------------	
	case DA_GROUP_CP_DATA:
		ins.type = COPROCESSOR;
		break;
	case DA_GROUP_CP_LS:
		ins.type = COPROCESSOR;
		break;
	case DA_GROUP_CP_REG:
		ins.type = COPROCESSOR;
		break;
		
	// ------------------------- DATA_PROC -----------------------------	
	case DA_GROUP_DATA_IMM:
		ins.type = DATA_PROC;
		ins.condition = args.data_imm.cond;
		ins.changed_regs[ args.data_imm.rd ] = 1;
		if ( (arm_data_instr_type)args.data_imm.op != MOV &&
			 (arm_data_instr_type)args.data_imm.op != MVN )
			 ins.used_regs[ args.data_imm.rn ] = 1;
		
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = args.data_imm.rd;}
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.change_flags = args.data_imm.flags;
		
		ins.data_instr_type = (arm_data_instr_type)args.data_imm.op;
		
		//ins.printType(); cout<<" | "; ins.printInstr(); cout<<" | IMM"<<endl;
		break;
	case DA_GROUP_DATA_IMM_SH:
		ins.type = DATA_PROC;
		ins.condition = args.data_imm_sh.cond;
		ins.changed_regs[ args.data_imm_sh.rd ] = 1;
		if ( (arm_data_instr_type)args.data_imm.op != MOV &&
			 (arm_data_instr_type)args.data_imm.op != MVN )
		{
			ins.used_regs[ args.data_imm_sh.rn ] = 1;
			if (args.data_imm_sh.rn == args.data_imm_sh.rm) ins.xor_dst = true;
		}
		ins.used_regs[ args.data_imm_sh.rm ] = 1;
		
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = args.data_imm_sh.rd;}
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.change_flags = args.data_imm_sh.flags;
		
		ins.data_instr_type = (arm_data_instr_type)args.data_imm.op;
		// FOR GET IMM VALUE
		//ins.printType(); cout<<" | "; ins.printInstr(); cout<<endl; cout<<" | IMM_SH"<<endl;
		break;
	case DA_GROUP_DATA_REG_SH:
		if ( args.data_reg_sh.rs == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.type = DATA_PROC;
		ins.condition = args.data_reg_sh.cond;
		ins.changed_regs[ args.data_reg_sh.rd ] = 1;
		if ( (arm_data_instr_type)args.data_imm.op != MOV &&
			 (arm_data_instr_type)args.data_imm.op != MVN )
		{
			ins.used_regs[ args.data_reg_sh.rn ] = 1;
			if (args.data_reg_sh.rn == args.data_reg_sh.rm) ins.xor_dst = true;
		}
		ins.used_regs[ args.data_reg_sh.rm ] = 1;
		ins.used_regs[ args.data_reg_sh.rs ] = 1;
		
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = args.data_reg_sh.rd;}
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.change_flags = args.data_reg_sh.flags;
		
		ins.data_instr_type = (arm_data_instr_type)args.data_imm.op;
		// FOR GET IMM VALUE
		//ins.printType(); cout<<" | "; ins.printInstr(); cout<<endl; cout<<" | REG_SH"<<endl;
		break;
	
	// ------------------------- DSP -----------------------------		
	case DA_GROUP_DSP_ADD_SUB:
		ins.type = DSP;
		ins.condition = args.dsp_add_sub.cond;
		ins.changed_regs[ args.dsp_add_sub.rd ] = 1;
		ins.used_regs[ args.dsp_add_sub.rn ] = 1;
		ins.used_regs[ args.dsp_add_sub.rm ] = 1;
		
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = args.dsp_add_sub.rd;}
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_DSP_MUL:
		if ( args.dsp_mul.rd == 15 || args.dsp_mul.rn == 15 || 
			 args.dsp_mul.rs == 15 || args.dsp_mul.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.dsp_mul.rd == args.dsp_mul.rm )
			{ ins.type = _INCORRECT; break; }
		ins.type = DSP;
		ins.condition = args.dsp_mul.cond;
		ins.changed_regs[ args.dsp_mul.rd ] = 1;
		ins.used_regs[ args.dsp_mul.rn ] = 1;
		ins.used_regs[ args.dsp_mul.rm ] = 1;
		ins.used_regs[ args.dsp_mul.rs ] = 1;
		
		//if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	// ------------------------- LOAD/STORE -----------------------------		
	case DA_GROUP_L_SIGN_IMM:
		if ( args.l_sign_imm.write && args.l_sign_imm.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.l_sign_imm.cond;
		ins.used_regs[ args.l_sign_imm.rn ] = 1;
		ins.dest_reg = args.l_sign_imm.rn;
		
		ins.type = LOAD;
		ins.changed_regs[ args.l_sign_imm.rd ] = 1;
		
		if ( args.l_sign_imm.write )
			ins.changed_regs[ args.l_sign_imm.rn ] = 1;
			
		if ( ins.used_regs[15] ) //ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_L_SIGN_REG:
		if ( args.l_sign_reg.write && args.l_sign_reg.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.l_sign_reg.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.l_sign_reg.p == 0 && args.l_sign_reg.rd == args.l_sign_reg.rm )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.l_sign_reg.cond;
		ins.used_regs[ args.l_sign_reg.rn ] = 1;
		ins.used_regs[ args.l_sign_reg.rm ] = 1;
		
		ins.dest_reg = args.l_sign_reg.rn;
		
		ins.type = LOAD;
		ins.changed_regs[ args.l_sign_reg.rd ] = 1;
		
		if ( args.l_sign_reg.write )
			ins.changed_regs[ args.l_sign_reg.rn ] = 1;
		
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_HW_IMM:
		if ( args.ls_hw_imm.write && args.ls_hw_imm.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_hw_imm.cond;
		ins.used_regs[ args.ls_hw_imm.rn ] = 1;
		
		ins.dest_reg = args.ls_hw_imm.rn;
		
		if ( args.ls_hw_imm.load )
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_hw_imm.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_hw_imm.rd ] = 1;
		}
		
		if ( args.ls_hw_imm.write )
			ins.changed_regs[ args.ls_hw_imm.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_HW_REG:
		if ( args.ls_hw_reg.write && args.ls_hw_reg.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_hw_reg.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_hw_reg.p == 0 && args.ls_hw_reg.rd == args.ls_hw_reg.rm )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_hw_reg.cond;
		ins.used_regs[ args.ls_hw_reg.rn ] = 1;
		ins.used_regs[ args.ls_hw_reg.rm ] = 1;
		
		ins.dest_reg = args.ls_hw_reg.rn;
		
		if ( args.ls_hw_reg.load )
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_hw_reg.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_hw_reg.rd ] = 1;
		}
		
		if ( args.ls_hw_reg.write )
			ins.changed_regs[ args.ls_hw_reg.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_IMM:
		if ( args.ls_imm.w && args.ls_imm.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_imm.cond;
		ins.used_regs[ args.ls_imm.rn ] = 1;
		
		ins.dest_reg = args.ls_imm.rn;
		
		if ( args.ls_imm.load )
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_imm.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_imm.rd ] = 1;
		}
		
		if ( args.ls_imm.w )
			ins.changed_regs[ args.ls_imm.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_MULTI:
		if ( args.ls_multi.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_multi.reglist == 0 )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_multi.cond;
		ins.used_regs[ args.ls_multi.rn ] = 1;
		
		ins.dest_reg = args.ls_multi.rn;
		
		if ( args.ls_multi.load )
		{
			ins.type = LOAD;
			ins.changed_regs |= get_arm_reglist( args.ls_multi.reglist );
		}
		else
		{
			ins.type = STORE;
			ins.used_regs |= get_arm_reglist( args.ls_multi.reglist );
		}
		
		if ( args.ls_multi.write )
		{
			/*if ( ins.type == STORE && ins.used_regs[ args.ls_multi.rn ] == 1 )
			{ ins.type = _INCORRECT; break; }*/
			ins.changed_regs[ args.ls_multi.rn ] = 1;	
		}
	
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.index_reg = args.ls_multi.rn;
		break;
		
	case DA_GROUP_LS_REG:
		if ( args.ls_reg.write && args.ls_reg.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_reg.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_reg.p == 0 && args.ls_reg.rd == args.ls_reg.rm )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_reg.cond;
		ins.used_regs[ args.ls_reg.rn ] = 1;
		ins.used_regs[ args.ls_reg.rm ] = 1;
		
		ins.dest_reg = args.ls_reg.rn;
		
		if ( args.ls_reg.load )
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_reg.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_reg.rd ] = 1;
		}
		
		if ( args.ls_reg.write )
			ins.changed_regs[ args.ls_reg.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_TWO_IMM:
		if ( args.ls_two_imm.write && args.ls_two_imm.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_two_imm.cond;
		ins.used_regs[ args.ls_two_imm.rn ] = 1;
		
		ins.dest_reg = args.ls_two_imm.rn;
		
		if ( args.ls_two_imm.store )//inverse
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_two_imm.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_two_imm.rd ] = 1;
		}
		
		if ( args.ls_two_imm.write )
			ins.changed_regs[ args.ls_two_imm.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_LS_TWO_REG:
		if ( args.ls_two_reg.write && args.ls_two_reg.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_two_reg.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.ls_two_reg.p == 0 && args.ls_two_reg.rd == args.ls_two_reg.rm )
			{ ins.type = _INCORRECT; break; }
		
		ins.condition = args.ls_two_reg.cond;
		ins.used_regs[ args.ls_two_reg.rn ] = 1;
		ins.used_regs[ args.ls_two_reg.rm ] = 1;
		
		ins.dest_reg = args.ls_two_reg.rn;
		
		if ( args.ls_two_reg.store )//inverse
		{
			ins.type = LOAD;
			ins.changed_regs[ args.ls_two_reg.rd ] = 1;
		}
		else
		{
			ins.type = STORE;
			ins.used_regs[ args.ls_two_reg.rd ] = 1;
		}
		
		if ( args.ls_two_reg.write )
			ins.changed_regs[ args.ls_two_reg.rn ] = 1;
			
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	// ------------------------- MSR (DATA_PROC) -----------------------------		
	case DA_GROUP_MRS:
		if ( args.mrs.rd == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.type = DATA_PROC;
		ins.condition = args.mrs.cond;
		ins.changed_regs[ args.mrs.rd ] = 1;
		
		//if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_MSR:
		if ( args.msr.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		
		ins.type = DATA_PROC;
		ins.condition = args.msr.cond;
		ins.used_regs[ args.msr.rm ] = 1;
		
		if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	case DA_GROUP_MSR_IMM:
		ins.type = DATA_PROC;
		ins.condition = args.msr_imm.cond;
		break;
		
	// ------------------------- MUL (DATA_PROC) -----------------------------	
	case DA_GROUP_MUL:
		if ( args.mul.rd == 15 || args.mul.rn == 15 || 
			 args.mul.rs == 15 || args.mul.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.mul.rd == args.mul.rm )
			{ ins.type = _INCORRECT; break; }
	 
		ins.type = DATA_PROC;
		ins.condition = args.mul.cond;
		ins.changed_regs[ args.mul.rd ] = 1;
		ins.used_regs[ args.mul.rn ] = 1;
		ins.used_regs[ args.mul.rs ] = 1;
		ins.used_regs[ args.mul.rm ] = 1;
		
		//if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.change_flags = args.mul.flags;
		break;
		
	case DA_GROUP_MULL:
		if ( args.mull.rd_hi == 15 || args.mull.rd_lo == 15 || 
			 args.mull.rs == 15 || args.mull.rm == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.mull.rd_hi == args.mull.rd_lo ||
			 args.mull.rd_hi == args.mull.rm ||
			 args.mull.rd_lo == args.mull.rm )
			{ ins.type = _INCORRECT; break; }
			
		ins.type = DATA_PROC;
		ins.condition = args.mull.cond;
		ins.changed_regs[ args.mull.rd_hi ] = 1;
		ins.changed_regs[ args.mull.rd_lo ] = 1;
		ins.used_regs[ args.mull.rs ] = 1;
		ins.used_regs[ args.mull.rm ] = 1;
		
		//if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		ins.change_flags = args.mull.flags;
		break;
	
	// ------------------------- SVC (DATA_PROC) -----------------------------		
	case DA_GROUP_SWI:
		ins.type = SVC;
		ins.condition = args.swi.cond;
		ins.used_regs[ 7 ] = 1;
		break;
	
	// ------------------------- SWP (DATA_PROC) -----------------------------		
	case DA_GROUP_SWP:
		if ( args.swp.rd == 15 || args.swp.rm == 15 || args.swp.rn == 15 )
			{ ins.type = _INCORRECT; break; }
		if ( args.swp.rn == args.swp.rm ||
			 args.swp.rn == args.swp.rd )
			{ ins.type = _INCORRECT; break; }
			
		ins.type = DATA_PROC;
		ins.condition = args.swp.cond;
		ins.changed_regs[ args.swp.rd ] = 1;
		ins.used_regs[ args.swp.rm ] = 1;
		ins.used_regs[ args.swp.rn ] = 1;
		
		//if ( ins.used_regs[15] ) ins.get_pc = true;
		if ( ins.changed_regs[15] ) ins.change_pc = true;
		break;
		
	// ------------------------- UNDEFINED -----------------------------
	case DA_GROUP_UNDEF_1:
	case DA_GROUP_UNDEF_2:
	case DA_GROUP_UNDEF_3:
	case DA_GROUP_UNDEF_4:
	case DA_GROUP_UNDEF_5:
	default:
		ins.type = _UNDEFINED;
		break;
	}// ====================== END OF SWITCH ============================
	
	if ( ins.condition == DA_COND_NV )
		ins.type = _INCORRECT;
	
	return ins;
	
}


// ********************************** THUMB ************************************************

/*instruct_thumb*/instruct_risc 	Lib_wrapper::thumb_get_instr(const unsigned char* data, int big_endian, int operand_size)
{
	/*instruct_thumb*/instruct_risc ins;
	_DInst insts[1] = {0};
    _DecomposeInfo info = {0};
	
    info.address = 1;
    info.code = data;//buf;
    info.codeLength = operand_size;//THUMB_SIZE;
	if ( big_endian ) 	info.endianity = ENDIANITY_LITTLE;
	else 				info.endianity = ENDIANITY_BIG;
    info.maxInstructions = 1;
    info.instructions = insts;
	
    armstorm_decompose(&info);
	
	/*TEST*/
	/*
	if ( ins.type != _UNDEFINED )
	{
		_TInst t = {0}; 
		armstorm_format(&info, &insts[0], &t);
		printf("%08llx %s %s", t.address, t.hex, t.instruction);
		
	}
	//else printf ("UNDEFINED");
	*/
	ins.info = info;
	ins.insts = insts[0];
	//ins._info = &info;
	//ins._insts = &insts[0];
	/*TEST*/
	
	ins.size = info.codeLength;//THUMB_SIZE;
	ins.reg_has_pc = -1;
	ins.isARM = false;
	ins.dest_reg = -1;
	ins.xor_dst = false;
	ins.condition = DA_COND_AL;
	// ====================== START OF SWITCH ============================
	switch ( insts[0].opcode ){
	
	// ----------------------- UNDEFINED -----------------------------
	case I_UNDEFINED:
		ins.type = _UNDEFINED;
		break;
	case I_BKPT:
		ins.type = _UNDEFINED;
		break;
		
	// ----------------------- DATA PROCESSING -----------------------------	
	// LOGICAL ------------
	case I_AND: if (insts[0].opcode==I_AND) ins.data_instr_type = _AND;
	case I_EOR: if (insts[0].opcode==I_EOR) ins.data_instr_type = EOR;
	case I_MVN: if (insts[0].opcode==I_MVN) ins.data_instr_type = MVN;
	case I_ORR: if (insts[0].opcode==I_ORR) ins.data_instr_type = ORR;
	case I_ROR: if (insts[0].opcode==I_ROR) ins.data_instr_type = ROR;
	case I_BIC: if (insts[0].opcode==I_BIC) ins.data_instr_type = BIC;
		ins.type = DATA_PROC;
		ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = insts[0].operands[0].value;}
		break;
		
	// SHIFTS ------------
	case I_ASR: if (insts[0].opcode==I_ASR) ins.data_instr_type = ASR;
	case I_LSL: if (insts[0].opcode==I_LSL) ins.data_instr_type = LSL;
	case I_LSR: if (insts[0].opcode==I_LSR) ins.data_instr_type = LSR;
		ins.type = DATA_PROC;
		ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = insts[0].operands[0].value;}
		break;

	// ARIPHMETIC ------------
	case I_ADC: if (insts[0].opcode==I_ADC) ins.data_instr_type = ADC;
	case I_ADD: if (insts[0].opcode==I_ADD) ins.data_instr_type = ADD;
	case I_SUB: if (insts[0].opcode==I_SUB) ins.data_instr_type = SUB;
	case I_NEG: if (insts[0].opcode==I_NEG) ins.data_instr_type = NEG;
	case I_SBC: if (insts[0].opcode==I_SBC) ins.data_instr_type = SBC;
	case I_MUL: if (insts[0].opcode==I_MUL) ins.data_instr_type = MUL;
		ins.type = DATA_PROC;
		ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( insts[0].operands[2].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[2].value ] = 1;
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = insts[0].operands[0].value;}
		
		if ( insts[0].operands[0].value == insts[0].operands[1].value ) ins.xor_dst = true;
		break;
		
	// MOV ------------
	case I_MOV: if (insts[0].opcode==I_MOV) ins.data_instr_type = MOV;
		ins.type = DATA_PROC;
		ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( ins.used_regs[15] ) {ins.get_pc = true; ins.reg_has_pc = insts[0].operands[0].value;}
		break;
		
	// COMPARE ------------
	case I_TST: if (insts[0].opcode==I_TST) ins.data_instr_type = TST;
	case I_CMN: if (insts[0].opcode==I_CMN) ins.data_instr_type = CMN;
	case I_CMP: if (insts[0].opcode==I_CMP) ins.data_instr_type = CMP;
		ins.type = DATA_PROC;
		ins.change_flags = true;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		break;
		
	// ----------------------- BRANCH -----------------------------
	case I_B:
		ins.type = BRANCH;
		ins.branch_offset = insts[0].target;
		ins.visible_branch_address = true;
		break;
		
	// ----------------------- BRANCH (conditional) -----------------------------
	case I_BAL: ins.condition = DA_COND_AL;
	case I_BCC: ins.condition = DA_COND_CC;
	case I_BCS: ins.condition = DA_COND_CS;
	case I_BEQ: ins.condition = DA_COND_EQ;
	case I_BGE: ins.condition = DA_COND_GE;
	case I_BGT: ins.condition = DA_COND_GT;
	case I_BHI: ins.condition = DA_COND_HI;
	case I_BMI: ins.condition = DA_COND_MI;
	case I_BNE: ins.condition = DA_COND_NE;
	case I_BPL: ins.condition = DA_COND_PL;
	case I_BVC: ins.condition = DA_COND_VC;
	case I_BVS: ins.condition = DA_COND_VS;
		ins.type = BRANCH;
		ins.branch_offset = insts[0].target;
		ins.visible_branch_address = true;
		break;
		
	// ----------------------- BRANCH LINK -----------------------------
	case I_BL:
	case I_BLE:
	case I_BLS:
	case I_BLT:
		ins.type = BRANCH_LINK;
		ins.branch_offset = insts[0].target;
		ins.visible_branch_address = true;
		ins.changed_regs[ 14 ] = 1;
		break;
		
	// ----------------------- BRANCH EXCHANGE -----------------------------
	case I_BLX:
		ins.type = BRANCH_LINK_EXCHANGE;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		ins.changed_regs[ 14 ] = 1;
		break;
	case I_BX:
		ins.type = BRANCH_EXCHANGE;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		break;
	
	// ----------------------- LOAD -----------------------------
	case I_LDR:
	case I_LDRB:
	case I_LDRH:
	case I_LDRSB:
	case I_LDRSH:
		ins.type = LOAD;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( insts[0].operands[2].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[2].value ] = 1;
		//if ( ins.used_regs[13] ) ins.changed_regs[13] = 1;
		break;
	// ----------------------- LOAD MULTIPLE -----------------------------
	case I_LDMIA:
		if ( insts[0].operands[1].value == 0 )
			{ ins.type = _INCORRECT; break; }
			
		ins.type = LOAD;
		ins.changed_regs = get_thumb_reglist( insts[0].operands[1].value );
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		ins.index_reg = insts[0].operands[0].value;
		break;
	// ----------------------- LOAD MULTIPLE (from stack) ------------------	
	case I_POP:
		if ( insts[0].operands[0].value == 0 )
			{ ins.type = _INCORRECT; break; }
		
		ins.type = LOAD;
		ins.changed_regs = get_thumb_reglist( insts[0].operands[0].value );
		if ( insts[0].operands[1].value != -1 )
			ins.changed_regs[ insts[0].operands[1].value ] = 1;
		ins.changed_regs[13] = 1;
		ins.used_regs[13] = 1;
		break;
		
	// ----------------------- STORE ------------------------------------
	case I_STR:
	case I_STRB:
	case I_STRH:
		ins.type = STORE;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		ins.dest_reg = insts[0].operands[0].value;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		if ( insts[0].operands[2].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[2].value ] = 1;
		//if ( ins.used_regs[13] ) ins.changed_regs[13] = 1;
		break;
	// ----------------------- STORE MULTIPLE -----------------------------	 
	case I_STMIA:
		if ( insts[0].operands[1].value == 0 )
			{ ins.type = _INCORRECT; break; }
			
		ins.type = STORE;
		ins.used_regs = get_thumb_reglist( insts[0].operands[1].value );
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		ins.used_regs[ insts[0].operands[0].value ] = 1;
		ins.index_reg = insts[0].operands[0].value;
		ins.dest_reg = insts[0].operands[0].value;
		break;
	// ----------------------- STORE MULTIPLE (from stack) ------------------
	case I_PUSH:
		if ( insts[0].operands[0].value == 0 )
			{ ins.type = _INCORRECT; break; }
			
		ins.type = STORE;
		ins.used_regs = get_thumb_reglist( insts[0].operands[0].value );
		if ( insts[0].operands[1].value == -1 )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		ins.changed_regs[13] = 1;
		ins.used_regs[13] = 1;
		break;
		
	// ----------------------- SVC -----------------------------
	case I_SWI:
		ins.type = SVC;
		ins.used_regs[ 7 ] = 1;
		break;
	
	// ----------------------- REVERSE -----------------------------
	case I_REV:
	case I_REV16:
	case I_REVSH:
		ins.data_instr_type = OTHER;
		ins.type = DATA_PROC;
		//ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		break;
	
	// ----------------------- EXTEND -----------------------------
	case I_SXTB:
	case I_SXTH:
	case I_UXTB:
	case I_UXTH:
		ins.data_instr_type = OTHER;
		ins.type = DATA_PROC;
		//ins.change_flags = true;
		ins.changed_regs[ insts[0].operands[0].value ] = 1;
		if ( insts[0].operands[1].type == OPERAND_REG )
			ins.used_regs[ insts[0].operands[1].value ] = 1;
		break;
	
	default:
		ins.type = _UNDEFINED;
		break;
	}// =================== END OF SWITCH ==============================
	
	if ( ins.used_regs[15] ) ins.get_pc = true;
	if ( ins.changed_regs[15] ) ins.change_pc = true;
	
	return ins;
}

// =========================================== PRINTING ===================================

void instruct_risc::printInstr()
{
	if ( type == _UNDEFINED )
		{printf ("UNDEFINED"); return;}
	if ( type == _INCORRECT )
		{printf ("INCORRECT"); return;}
		
	if (isARM)
	{
		da_instr_fprint(stdout, &instr, &args, 0);
		//printf("\n");
	}
	else
	{
		_TInst t = {0}; 
		armstorm_format(&info, &insts, &t);
		printf(/*"%08llx*/"%s %s", /*t.address,*/ t.hex, t.instruction);
	}
}

void instruct_risc::printType()
{
	if (isARM) cout<<"(A) ";
	else	   cout<<"(T) ";
	switch (type)
	{
		case BRANCH_LINK_EXCHANGE:
			cout << "BRANCH_LINK_EXCHANGE";
			break;
		case BRANCH_EXCHANGE:
			cout << "BRANCH_EXCHANGE";
			break;
		case BRANCH_LINK:
			cout << "BRANCH_LINK";
			break;
		case BRANCH:
			cout << "BRANCH";
			break;

		case LOAD:
			cout << "LOAD";
			break;
		case STORE:
			cout << "STORE";
			break;

		case DATA_PROC:
			switch (data_instr_type)
			{
				case _AND:
					cout << "AND";
					break;
				case EOR:
					cout << "EOR";
					break;
				case SUB:
					cout << "SUB";
					break;
				case RSB:
					cout << "RSB";
					break;
				case ADD:
					cout << "ADD";
					break;
				case ADC:
					cout << "ADC";
					break;
				case SBC:
					cout << "SBC";
					break;
				case RSC:
					cout << "RSC";
					break;
				case TST:
					cout << "TST";
					break;
				case TEQ:
					cout << "TEQ";
					break;
				case CMP:
					cout << "CMP";
					break;
				case CMN:
					cout << "CMN";
					break;
				case ORR:
					cout << "ORR";
					break;
				case MOV:
					cout << "MOV";
					break;
				case BIC:
					cout << "BIC";
					break;
				case MVN:
					cout << "MVN";
					break;
				case MAX:
					cout << "MAX";
					break;

				// for thumb
				case ROR:
					cout << "ROR";
					break;
				case ASR:
					cout << "ASR";
					break;
				case LSL:
					cout << "LSL";
					break;
				case LSR:
					cout << "LSR";
					break;
				case NEG:
					cout << "NEG";
					break;
				case MUL:
					cout << "MUL";
					break;
				case OTHER:
					cout << "OTHER";
					break;
			}
			break;

		case COPROCESSOR:
			cout << "COPROCESSOR";
			break;
		case DSP:
			cout << "DSP";
			break;
		case SVC:
			cout << "SVC";
			break;

		case _UNDEFINED:
			cout << "UNDEFINED";
			break;
		case _INCORRECT:
			cout << "INCORRECT";
			break;
		default:
			cout << "Type=" << data_instr_type;
	}
	
	cout << " use: ";
	for (int i=0; i<16; i++)
		if ( used_regs[i] ) cout << "r" << i;
		
	cout << " change: ";
	for (int i=0; i<16; i++)
		if ( changed_regs[i] ) cout << "r" << i;
	
	cout<<" | Offset= "<<offset<<" Branch= "<<branch_offset;
}

void instruct_risc::printOffset()
{
	cout<<"Offset= "<<offset;
}











