#ifndef _FLOW_THUMB_H
#define _FLOW_THUMB_H

#pragma once
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

//#define MAX_DATA_LEN 256
//#define MIN_DATA_LEN 64

//#include "libdisarm/disarm.h"
#include "lib_wrapper.h"
/*
struct oper_arm
{
        enum Operand type;
        int reg;
        DWORD immediate;
};
*/
typedef std::vector< std::vector< instruct_thumb > > fl_type_thumb;

//TODO: fix Flow class ( make data fields private )
class Flow_thumb
{
private:
	/**function dissasemles flow and fills fl field
	 * @param buffer - given source buffer
	 * @param length - length of buffer
	 * @param offset - offset from with to start disassembling
	 */
	void disassemble_flow(const unsigned char *buffer , unsigned length, int off);
	
	/** initialize offsets and ext_offset array */
	void init();
public:
    Flow_thumb();
	Flow_thumb(const unsigned char* buffer, int length );
	fl_type_thumb fl;
	
	//first item in pair stands for chain number, second for offset in the chain
	std::pair< int, int > offsets[MAX_DATA_LEN];
	bool ext_offset[MAX_DATA_LEN];
//	std::map< int, ParCh > ParentChildArray;

//additional function
	void insertChildAndParent( int o, int addr );
	bool doJmp( int o, int addr );
	//Flow_thumb& generateEIFG();

//debugging function
	void printFamilyArray();
};


#endif /* _FLOW_THUMB_H */


