#ifndef _FLOW_ARM_H
#define _FLOW_ARM_H

#pragma once
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <stdint.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//#define MAX_DATA_LEN 256
#define MAX_DATA_LEN 1408
#define MIN_DATA_LEN 64

#define MAX_JUMP 100

#define MIN_DISAS_LEN 0

#define ARM_SIZE 4
#define THUMB_SIZE 2

#include "lib_wrapper.h"

typedef std::vector< std::vector< instruct_risc > > fl_type_ARM;
//typedef std::vector< std::vector< instruct_arm > > fl_type_arm;
//typedef std::vector< std::vector< instruct_thumb > > fl_type_thumb;

class Flow_ARM{
public:
	Flow_ARM();
	virtual ~Flow_ARM();
	bool check_complex_payload(instruct_risc ins);
	
	fl_type_ARM fl;
	
	std::pair< int, int > offsets[MAX_DATA_LEN];
	bool ext_offset[MAX_DATA_LEN];
	
	//void printFlow();
};

class Flow_arm
	: public Flow_ARM{
private:
	int disassemble_flow(const unsigned char *buffer , unsigned length, int off);
	void init();
public:
    Flow_arm();
	Flow_arm(const unsigned char* buffer, int length );
	~Flow_arm();
};

class Flow_thumb
	: public Flow_ARM{
private:
	int disassemble_flow(const unsigned char *buffer , unsigned length, int off);
	void init();
public:
    Flow_thumb();
	Flow_thumb(const unsigned char* buffer, int length );
	~Flow_thumb();
};

#endif /* _FLOW_ARM_H */



