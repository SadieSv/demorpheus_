
#include "classifier_arm.h"
#include "support_arm.h"

using namespace std;

boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
boost::shared_ptr<APE_ARM> APE_classifier;
boost::shared_ptr<EmulBased_ARM> emul_ARM;
int _buffer_len;
vector< vector< pair<int,int> > > offsets_APE;

//boost::shared_ptr<APE_ARM> APE_classifier;
//boost::shared_ptr< vector<Flow_ARM*> > flow_ARM;
//boost::shared_ptr<Flow_arm> flow_arm;
//boost::shared_ptr<Flow_thumb> flow_thumb;

//----------------------------RegInit----------------------------
RegInit::RegInit()
{
	for (int i=0; i<15; i++)
	{
		regs[i][13] = regs[i][15] = 1;
	}
}

void RegInit::Sbit()
{
	if ( m != THUMB_MODE )
	{
		tmp.reset();
		
		for (int i=0; i<15; i++)
		{
			tmp |= regs[i];
		}
		
		for (int i=0; i<15; i++)
		{
			regs[i] = tmp;
		}
	}
}

void RegInit::reset(int cond)
{
	if (cond < 14)
	{
		regs[cond].reset();
		regs[cond][13] = regs[cond][15] = 1;
	}
	
	else//if ( cond == 14 )
	{
		for (int i=0; i<=14; i++)
		{
			regs[i].reset();
			regs[i][13] = regs[i][15] = 1;
		}
	}
}

void RegInit::ALcond( int cond )
{
	if ( m != THUMB_MODE )
	{
		if ( cond == 14 )
			for (int i=0; i<=14; i++)
			{
				regs[i] |= regs[14];
			}
	}
}

std::bitset<16>& RegInit::operator[]( int cond )
{
	if ( cond >14 || cond < 0 ) return regs[14];
	return regs[cond];
}

std::bitset<16> RegInit::get( int cond )
{
	if ( cond >14 || cond < 0 ) return regs[14];
	
	if ( m != THUMB_MODE )
	{
		if (cond < 14)
		{
			return regs[cond];
		}
		
		if ( cond == 14 )
		{
			tmp.reset();
			
			for (int i=0; i<=14; i++)
			{
				tmp |= regs[i];
			}
			return tmp;
		}
	}
	return regs[cond];
}

//=======================FLOW BASED ARM PARENT CLASS==========================================//
void FlowBased_ARM::init()
{
	// Make disassemble flows
	flow_ARM = boost::shared_ptr< vector<Flow_ARM*> >(new vector<Flow_ARM*>(2));
	flow_ARM->at(0) = (Flow_ARM*)(new Flow_arm(_buffer, data_len));
	flow_ARM->at(1) = (Flow_ARM*)(new Flow_thumb(_buffer, data_len));
	
	// Make APE result
	APE_classifier = boost::shared_ptr<APE_ARM>(new APE_ARM());
	APE_classifier->APE_check();
	
	// Make emulator
	emul_ARM = boost::shared_ptr<EmulBased_ARM>( new EmulBased_ARM());
	emul_ARM->Emul_check(_buffer, data_len);
}
void FlowBased_ARM::init(const unsigned char* _buffer, int data_len)
{
	flow_ARM = boost::shared_ptr< vector<Flow_ARM*> >(new vector<Flow_ARM*>(2));
	flow_ARM->at(0) = (Flow_ARM*)(new Flow_arm(_buffer, data_len));
	flow_ARM->at(1) = (Flow_ARM*)(new Flow_thumb(_buffer, data_len));
	
	APE_classifier = boost::shared_ptr<APE_ARM>(new APE_ARM());
	APE_classifier->APE_check();
	
	emul_ARM = boost::shared_ptr<EmulBased_ARM>( new EmulBased_ARM());
	emul_ARM->Emul_check(_buffer, data_len);
	
	_buffer_len = data_len;
}
void FlowBased_ARM::init_only(const unsigned char* _buffer, int data_len)
{
	flow_ARM = boost::shared_ptr< vector<Flow_ARM*> >(new vector<Flow_ARM*>(2));
	flow_ARM->at(0) = (Flow_ARM*)(new Flow_arm(_buffer, data_len));
	flow_ARM->at(1) = (Flow_ARM*)(new Flow_thumb(_buffer, data_len));
}

// ---------------------------------- INIT STRUCTURES ---------------------------------------------
void FlowBased_ARM::init_disassemble()
{
	// Make disassemble flows
	flow_ARM = boost::shared_ptr< vector<Flow_ARM*> >(new vector<Flow_ARM*>(2));
	flow_ARM->at(0) = (Flow_ARM*)(new Flow_arm(_buffer, data_len));
	flow_ARM->at(1) = (Flow_ARM*)(new Flow_thumb(_buffer, data_len));
}

void FlowBased_ARM::init_APE()
{
	// Make APE result
	APE_classifier = boost::shared_ptr<APE_ARM>(new APE_ARM());
	APE_classifier->APE_check();
}

void FlowBased_ARM::init_emulator()
{
	// Make emulator
	emul_ARM = boost::shared_ptr<EmulBased_ARM>( new EmulBased_ARM());
	emul_ARM->Emul_check(_buffer, data_len);
}

void FlowBased_ARM::reload()
{
	init();
}

void FlowBased_ARM::release()
{
	if ( (Flow_arm*)flow_ARM->at(0) != NULL )
		delete (Flow_arm*)flow_ARM->at(0);
	if ( (Flow_thumb*)flow_ARM->at(1) != NULL )
		delete (Flow_thumb*)flow_ARM->at(1);
	
	//flow_ARM.reset();
	
	//APE_classifier.reset();
}

//========================DISAS LENGTH CLASSIFIER=========================//
DisasLength_ARM::DisasLength_ARM()
{
	fn = 0.05;
	fp_r = 0.67;
	fp_m = 0.95;
	complexity = 12;
}

int DisasLength_ARM::getMinSize_arm(double x)
{
	return 0.0000000050124*x*x*x - 0.0000205259*x*x + 0.0340257*x + 13.9291;
}
int DisasLength_ARM::getMinSize_thumb(double x)
{
	return 0.00000000416336*x*x*x - 0.0000286278*x*x + 0.0494642*x + 14.2234;
}

bool DisasLength_ARM::check()
{
	long Instruction_Length;
	int arm_min_chain = getMinSize_arm(_buffer_len);
	int thumb_min_chain = getMinSize_thumb(_buffer_len);
	
	for (unsigned i = 0; i < flow_ARM->at(0)->fl.size(); i++ ) 
	{
		Instruction_Length = getChainSize_arm( i, 0 );
								
		if ( Instruction_Length >= arm_min_chain ) return true;
	}
	
	for (unsigned i = 0; i < flow_ARM->at(1)->fl.size(); i++ ) 
	{
		Instruction_Length = getChainSize_thumb( i, 0 );
								
		if ( Instruction_Length >= thumb_min_chain ) return true;
	}
	
	return false;
}

int DisasLength_ARM::getChainSize_arm( int i, int j ) const
{
		int size = flow_ARM->at(0)->fl[i].size();
        int offset =  flow_ARM->at(0)->fl[i][size-1].offset;

		if ( size - j >= _MIN_INSTR_CHAIN || offset < 0 || offset >= MAX_DATA_LEN ||
		  flow_ARM->at(0)->ext_offset[ flow_ARM->at(0)->fl[i][size-1].offset ]  == false ) return size - j;
        int f = flow_ARM->at(0)->offsets[ offset ].first;
        int s = flow_ARM->at(0)->offsets[ offset ].second;
        //infinite cycle
        if ( i == f ) return size-j;
        size += getChainSize_arm( f, s );
        return size-(j+1);
}

int DisasLength_ARM::getChainSize_thumb( int i, int j ) const
{
	int size = flow_ARM->at(1)->fl[i].size();
    int offset =  flow_ARM->at(1)->fl[i][size-1].offset;
	
	if ( size - j >= _MIN_INSTR_CHAIN || offset < 0 || offset >= MAX_DATA_LEN ||
		  flow_ARM->at(1)->ext_offset[ flow_ARM->at(1)->fl[i][size-1].offset ]  == false ) return size - j;
    int f = flow_ARM->at(1)->offsets[ offset ].first;
    int s = flow_ARM->at(1)->offsets[ offset ].second;
    //infinite cycle
    if ( i == f ) return size-j;
    size += getChainSize_thumb( f, s );
    return size-(j+1);
}

//========================APE CLASSIFIER===================================//

APE_ARM::APE_ARM()
{
	//APE_check();
}


bool APE_ARM::APE_check()
{
	bool check_success = false;
	int shellcode_type = 0;
	visited = vector< vector<bool> >(2);
	
	visited[0] = vector<bool>( flow_ARM->at(0)->fl.size() );
	visited[1] = vector<bool>( flow_ARM->at(1)->fl.size() );
	
	offsets_APE.clear();
	offsets_APE = vector< vector< pair<int,int> > >(2);
	
	bitset<16> regs;
	bitset<16> regs_has_pc;
	regs.reset();
	regs_has_pc.reset();
	regs[13] = regs[15] = 1;
	
	APE_flow_info tmp1;
	
	//---ARM---
	for (int i=0; i<flow_ARM->at(0)->fl.size(); i++)
	{
		check_success = false;
		shellcode_type = 0;
		
		//if ( flow_ARM->at(0)->fl[i].size() < _MIN_INSTR_CHAIN ) continue;
		
		if ( !visited[ARM_MODE][i] ) 
		{
			tmp1 = forward_flow(i,0,ARM_MODE,regs,regs_has_pc);
			visited[ARM_MODE][i] = true;
		}
		
		if ( tmp1.patterns_trust >= MIN_PATTERN_TRUST )
			{flow_info.patterns = check_success = true; shellcode_type|=3;}
		/*
		if ( tmp1.max_disas_len >= _MIN_INSTR_CHAIN )
			{flow_info.disas_len = check_success = true;}
		*/
		if ( tmp1.load_num >= 1 && tmp1.store_num >= 1 && tmp1.getpc >= 1 &&
			( tmp1.cycle_number >= 1 || tmp1.branch_use_pc >= 1 ) )			 	  
			{flow_info.decrypt_exist = check_success = true; shellcode_type|=2;}
		
		if ( tmp1.mode_change >= MAX_MODE_CHANGE )
			{flow_info.mode_change = check_success = true; shellcode_type|=3;}
		
		if (check_success) 
		{//cout<<"mode= "<<0<<" , offset= "<<flow_ARM->at(0)->fl[i][0].offset<<endl;
			offsets_APE[0].push_back( make_pair( flow_ARM->at(0)->fl[i][0].offset, shellcode_type ) );
		}
	}
	//---THUMB---
	for (int i=0; i<flow_ARM->at(1)->fl.size(); i++)
	{
		check_success = false;
		shellcode_type = 0;
		
		//if ( flow_ARM->at(1)->fl[i].size() < _MIN_INSTR_CHAIN ) continue;
		
		if ( !visited[THUMB_MODE][i] ) 
		{
			tmp1 = forward_flow(i,0,THUMB_MODE,regs,regs_has_pc);
			visited[THUMB_MODE][i] = true;
		}
		
		if ( tmp1.patterns_trust >= MIN_PATTERN_TRUST )
			{flow_info.patterns = check_success = true; shellcode_type|=3;}
		/*
		if ( tmp1.max_disas_len >= _MIN_INSTR_CHAIN )
			{flow_info.disas_len = check_success = true;}
		*/
		if ( tmp1.load_num >= 1 && tmp1.store_num >= 1 && tmp1.getpc >= 1 &&
			( tmp1.cycle_number >= 1 || tmp1.branch_use_pc >= 1 ) )			 	  
			{flow_info.decrypt_exist = check_success = true; shellcode_type|=2;}
		
		if ( tmp1.mode_change >= MAX_MODE_CHANGE )
			{flow_info.mode_change = check_success = true; shellcode_type|=3;}
		
		if (check_success) 
		{//cout<<"mode= "<<1<<" , offset= "<<flow_ARM->at(1)->fl[i][0].offset<<endl;
			offsets_APE[1].push_back( make_pair( flow_ARM->at(1)->fl[i][0].offset, shellcode_type ) );
		}
	}
	return false;
}

int APE_ARM::trust_pattern( bitset<16> regs, bitset<16> regs_has_pc )
{
		 if ( regs_has_pc[0] == 1 ) return APE_trust_2;
	else if ( regs_has_pc[1] == 1 ) return APE_trust_2;
	else if ( regs_has_pc[2] == 1 ) return APE_trust_2;
	/*
		 if ( regs[0] == 0 ) return APE_trust_0;
	else if ( regs[1] == 0 ) return APE_trust_1;
	else if ( regs[2] == 0 ) return APE_trust_2;
	else if ( regs[3] == 0 ) return APE_trust_3;
	else 					 return APE_trust_4;*/
}

bool APE_ARM::decrypt_exist(int i, int s, int j, int mode)
{
	bool load=false, store=false;
	for (int k=s; k<=j; k++)
	{
		if ( flow_ARM->at(mode)->fl[i][k].type == LOAD) load = true;
		if ( flow_ARM->at(mode)->fl[i][k].type == STORE) store = true;
	}
	return load & store;
}

APE_flow_info APE_ARM::forward_flow( int i, int _j, int mode, bitset<16> regs, bitset<16> regs_has_pc )
{
  int offset;
  int patterns = 0;
  int disas_len = 0;
  int BX_patterns = 0;
  //int sis_calls = 0;
  int trust;
  int j = _j;
  int cond;
  bool store_was = false, load_was = false, jump_was = false;
  APE_flow_info ret_vec;
  ret_vec.regs.reset();
  ret_vec.regs_has_pc.reset();
  /*
  for ( int k=0; k<16; k++ )
	ret_vec.regs[k] = regs[k];

  for ( int k=0; k<16; k++ )
	ret_vec.regs_has_pc[k] = regs_has_pc[k];
  *///cout<<"================="<<flow_ARM->at(mode)->fl[i].size()<<"========================"<<endl;
  
  ret_vec.regs = regs;
  ret_vec.regs_has_pc = regs_has_pc;
  
  if ( !visited[mode][i] )
  {
	visited[mode][i] = true;
  }
  else return ret_vec;
  
  int m = mode;
  {
	while ( i>=0 && i < flow_ARM->at(m)->fl.size() && j>=0 && j < flow_ARM->at(m)->fl[i].size() )
	{
		if ( flow_ARM->at(m)->fl[i][j].type != _UNDEFINED &&
			flow_ARM->at(m)->fl[i][j].type != _INCORRECT &&
			flow_ARM->at(m)->fl[i][j].ext_off == false )
		{
			ret_vec.inc_disas_len();
			//TEST
			//if (i<2)
			/*{
			cout<<"i= "<<i<<" j= "<<j<<" ";
			cout<<ret_vec.regs<<" | "<<ret_vec.regs_has_pc<<" | ";
			if (flow_ARM->at(m)->fl[i][j].reg_has_pc>=0 && flow_ARM->at(m)->fl[i][j].reg_has_pc<16  )
				cout<<flow_ARM->at(m)->fl[i][j].reg_has_pc<<" | ";
			else cout<<"N/A | ";
			flow_ARM->at(m)->fl[i][j].printType(); cout<<endl<<" | ";
			flow_ARM->at(m)->fl[i][j].printInstr(); cout<<endl;
			}*/
			//TEST
		}
	  
	    if ( flow_ARM->at(m)->fl[i][j].ext_off == false )
			switch ( flow_ARM->at(m)->fl[i][j].type )
			{
				case BRANCH_LINK_EXCHANGE: //===========================================================
					/*trust = trust_pattern( ret_vec.regs );
					ret_vec.patterns_trust += trust;
					ret_vec.patterns_num++;
					if (trust!=0) ret_vec.patterns_init++;*/
					// If have a immediate value
					if ( flow_ARM->at(m)->fl[i][j].visible_branch_address )
					{
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
						if ( offset > -1 && offset < MAX_DATA_LEN )
						{
							ret_vec.getpc++;
							ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
							
							int f = flow_ARM->at(!m)->offsets[ offset ].first;
							int s = flow_ARM->at(!m)->offsets[ offset ].second;
							if ( f != -1 && s != -1 ) // GOTO THUMB MODE
							{
								if ( /*f < i ||*/ f==i && s<=j)
								{
									if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
										ret_vec.cycle_number++;
									else
										jump_was = true;
								}
								ret_vec += forward_flow( f, s, !m, ret_vec.regs, ret_vec.regs_has_pc );
							}
						}
					}
					else
					{
						/*if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
									== flow_ARM->at(m)->fl[i][j].used_regs )
						{
							ret_vec.BX_patterns++;
							ret_vec.getpc++;
						}*/
						if ( (ret_vec.regs_has_pc & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
						{
							ret_vec.branch_use_pc += 1;
							offset = flow_ARM->at(m)->fl[i][j].offset;
							for ( int t=MAX_BX_GAP ; t>=0; t-- )
							{
								if (offset-t < 0) continue;
								int f = flow_ARM->at(!m)->offsets[ offset-t ].first;
								if ( (f != -1) )
								{
									ret_vec.mode_change++;
									ret_vec += forward_flow( f, 0, !m, ret_vec.regs, ret_vec.regs_has_pc );
									break;
								}
							}
						}
					}
					//ret_vec.regs[0] = 1;
					break;
					
				case BRANCH_EXCHANGE: //===========================================================
					// MAY CHANGE TO THUMB
					/*if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
									== flow_ARM->at(m)->fl[i][j].used_regs )
							ret_vec.BX_patterns++;*/
					if ( (ret_vec.regs_has_pc & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
					{
						ret_vec.branch_use_pc += 1;
						offset = flow_ARM->at(m)->fl[i][j].offset;
						for ( int t=MAX_BX_GAP ; t>=0; t-- )
						{
							if (offset-t < 0) continue;
							int f = flow_ARM->at(!m)->offsets[ offset-t ].first;
							if ( (f != -1) )
							{
								ret_vec.mode_change++;
								ret_vec += forward_flow( f, 0, !m, ret_vec.regs, ret_vec.regs_has_pc );
								break;
							}
						}
					}
					break;
					
				case BRANCH_LINK: //===========================================================
					/*trust = trust_pattern( ret_vec.regs );
					ret_vec.patterns_trust += trust;
					ret_vec.patterns_num++;
					if (trust!=0) ret_vec.patterns_init++;*/
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
					if ( offset > -1 && offset < MAX_DATA_LEN )
					{
						ret_vec.getpc++;
						ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
						
						int f = flow_ARM->at(m)->offsets[ offset ].first;
						int s = flow_ARM->at(m)->offsets[ offset ].second;
						if ( f != -1 && s != -1 )
						{
							if ( /*f < i ||*/ f==i && s<=j)
							{
								if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
									ret_vec.cycle_number++;
								else
									jump_was = true;
							}
							else ret_vec += forward_flow( f, s, m, ret_vec.regs, ret_vec.regs_has_pc );
						}
					}
					//ret_vec.regs[0] = 1;
					break;
					
				case BRANCH: //===========================================================
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
					if ( offset > -1 && offset < MAX_DATA_LEN )
					{	
						int f = flow_ARM->at(m)->offsets[ offset ].first;
						int s = flow_ARM->at(m)->offsets[ offset ].second;
						if ( f != -1 && s != -1 )
						{
							if ( /*f < i ||*/ f==i && s<=j)
							{
								if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
									ret_vec.cycle_number++;
								else
									jump_was = true;
							}
							else ret_vec += forward_flow( f, s, m, ret_vec.regs, ret_vec.regs_has_pc );
						}
					}
					break;
		
				case LOAD: //===========================================================
				case STORE: //===========================================================
					if ( flow_ARM->at(m)->fl[i][j].type == LOAD)
					{
						ret_vec.load_num++;
						load_was = false;
						
						if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
										== flow_ARM->at(m)->fl[i][j].used_regs )
						{
							ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
						}
						else
						{
							ret_vec.regs &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
							ret_vec.regs_has_pc &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
						}
					}
					else // STORE
					{
						ret_vec.store_num++;
						store_was = true;
						
						if ( flow_ARM->at(m)->fl[i][j].dest_reg != -1 )
						if ( ret_vec.regs_has_pc[flow_ARM->at(m)->fl[i][j].dest_reg] == 1
						/*( (ret_vec.regs_has_pc & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )*/ )
						{
							ret_vec.cycle_number++; // decrypt exist, my not be a cycle
						}
					}
					/*if ( jump_was && load_was && store_was )
						ret_vec.cycle_number++;*/
					/*
					if ( flow_ARM->at(m)->fl[i][j].type == LOAD)
					{
						ret_vec.load_num++;
						load_was = false;
					}
					else
					{
						ret_vec.store_num++;
						store_was = true;
					}*/
					/*
					if ( ( (ret_vec.regs_has_pc & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 ) &&
						 ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) )
					{
						ret_vec.usepc += USE_PC_RATE;
						if ( store_was )
							//ret_vec.cycle_number++; // decrypt exist, my not be a cycle
					}*/
					break;
		
				case DATA_PROC: //===========================================================
				case COPROCESSOR:
				case DSP:
					switch ( flow_ARM->at(m)->fl[i][j].data_instr_type )
					{
						// COMPARE
						case TST:
						case TEQ:
						case CMP:
						case CMN:
							break;
							
						// LOGICAL
						case EOR:
							if ( flow_ARM->at(m)->fl[i][j].used_regs == flow_ARM->at(m)->fl[i][j].changed_regs )
							{
								ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
								ret_vec.regs_has_pc &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
							}
							break;
						case SUB:
							if ( flow_ARM->at(m)->fl[i][j].xor_dst )
							{
								ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
								ret_vec.regs_has_pc &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
								break;
							}
						
						case ORR:
						case BIC:
						case _AND:
						
						// ARIPHMETIC
						case ADD:
						case ADC:
						case SBC:
						case RSB:				
						case RSC:
						
						// MOV
						case MOV:
						case MVN:
						default://cout<<" PC HAS = "<<flow_ARM->at(m)->fl[i][j].reg_has_pc<<endl;
							if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
									== flow_ARM->at(m)->fl[i][j].used_regs )
							{
								ret_vec.regs |= flow_ARM->at(m)->fl[i][j].changed_regs;
								if (flow_ARM->at(m)->fl[i][j].reg_has_pc != -1)
								{
									ret_vec.getpc++;
									ret_vec.regs_has_pc[flow_ARM->at(m)->fl[i][j].reg_has_pc] = 1;
									//flow_ARM->at(m)->fl[i][j].printInstr(); cout<<" | "<<ret_vec.regs_has_pc<<endl; 
								}
								else if ( (ret_vec.regs_has_pc & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
								{
									ret_vec.getpc++;
									ret_vec.regs_has_pc |= flow_ARM->at(m)->fl[i][j].changed_regs;
								}
								else if ( flow_ARM->at(m)->fl[i][j].used_regs.count() != 0 )
								{
									ret_vec.regs_has_pc &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
								}
							}
							else 
							{
								ret_vec.regs &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
								ret_vec.regs_has_pc &= ~flow_ARM->at(m)->fl[i][j].changed_regs;
							}
							break;
					}
					break;
		
				case SVC: //===========================================================
					if ( m == 0 || m == 1 && ret_vec.regs[7] == 1 )
					{
						trust = trust_pattern( ret_vec.regs, ret_vec.regs_has_pc );
						ret_vec.patterns_trust += trust;					
						if (trust!=0) ret_vec.svc_num++;
						// TEST
						/*
						if (trust != 0)
						{cout<<trust<<endl;
							cout<<ret_vec.regs<<" | "<<ret_vec.regs_has_pc<<" | "<<endl;
							for(int j=0; j<flow_ARM->at(m)->fl[i].size(); j++)
								if ( flow_ARM->at(m)->fl[i][j].ext_off != true )
								{
									flow_ARM->at(m)->fl[i][j].printType();cout<<" || ";
									flow_ARM->at(m)->fl[i][j].printInstr();cout<<endl;
								}
						}*/
						// TEST
						ret_vec.regs.reset();
						ret_vec.regs[0] = ret_vec.regs[13] = ret_vec.regs[15] = 1;
					}
					ret_vec.patterns_num++;
					break;
		
				case _UNDEFINED: //===========================================================
				case _INCORRECT:
				default:
					//ret_vec.dec_disas_len();
					//if (flow_ARM->at(m)->ext_offset[ flow_ARM->at(m)->fl[i][j].offset ] == false)
					if ( flow_ARM->at(m)->fl[i][j].ext_off == false )
						return ret_vec;
					break;
			}
		/*if ( (flow_ARM->at(m)->ext_offset[ flow_ARM->at(m)->fl[i][j].offset ] == true) &&
			 ( flow_ARM->at(m)->fl[i].size()-1 == j ) )*/
		if ( flow_ARM->at(m)->fl[i][j].ext_off == true )
		{
			offset = flow_ARM->at(m)->fl[i][j].offset;
			
			ret_vec.disas_len--;
			ret_vec += forward_flow( flow_ARM->at(m)->offsets[ offset ].first, 
									 flow_ARM->at(m)->offsets[ offset ].second, 
									 m, 
									 ret_vec.regs,
									 ret_vec.regs_has_pc 
									 );
									 
			return ret_vec;
		}
		else j++;
	}
  }
  return ret_vec;
}

////=========================APE FLAG CHECK parent class=========================//

APE_flag_check_ARM::APE_flag_check_ARM()
{
	//APE_check();
}


bool APE_flag_check_ARM::check()
{
	visited = vector< vector<bool> >(2);
	
	visited[0] = vector<bool>( flow_ARM->at(0)->fl.size() );
	visited[1] = vector<bool>( flow_ARM->at(1)->fl.size() );
	
	/*bitset<16>*/RegInit regs;
	/*bitset<16>*/RegInit regs_has_pc;
	regs.reset(14);
	regs_has_pc.reset(14);
	//regs[13] = regs[15] = 1;
	
	APE_flow_info_flag tmp1;
	
	//---ARM---
	for (int i=0; i<flow_ARM->at(0)->fl.size(); i++)
	{
		if ( !visited[ARM_MODE][i] ) 
		{
			tmp1 = forward_flow(i,0,ARM_MODE,regs,regs_has_pc);
			visited[ARM_MODE][i] = true;
		}
		
		if ( tmp1.patterns_trust >= MIN_PATTERN_TRUST )
			{/*flow_info.patterns =*/return true;}
		/*
		if ( tmp1.max_disas_len >= _MIN_INSTR_CHAIN )
			{flow_info.disas_len = true;}
		
		if ( tmp1.load_num >= 1 && tmp1.store_num >= 1 && tmp1.getpc >= 1 &&
			( tmp1.cycle_number >= 1 || tmp1.branch_use_pc >= 1 ) )			 	  
			{flow_info.decrypt_exist = true;}
		
		if ( tmp1.mode_change >= MAX_MODE_CHANGE )
			{flow_info.mode_change = true;}*/
	}
	//---THUMB---
	for (int i=0; i<flow_ARM->at(1)->fl.size(); i++)
	{
		if ( !visited[THUMB_MODE][i] ) 
		{
			tmp1 = forward_flow(i,0,THUMB_MODE,regs,regs_has_pc);
			visited[THUMB_MODE][i] = true;
		}
		
		if ( tmp1.patterns_trust >= MIN_PATTERN_TRUST )
			{/*flow_info.patterns =*/return true;}
		/*
		if ( tmp1.max_disas_len >= _MIN_INSTR_CHAIN )
			{flow_info.disas_len = true;}
		
		if ( tmp1.load_num >= 1 && tmp1.store_num >= 1 && tmp1.getpc >= 1 &&
			( tmp1.cycle_number >= 1 || tmp1.branch_use_pc >= 1 ) )			 	  
			{flow_info.decrypt_exist = true;}
		
		if ( tmp1.mode_change >= MAX_MODE_CHANGE )
			{flow_info.mode_change = true;}*/
	}
	return false;
}

int APE_flag_check_ARM::trust_pattern( /*bitset<16>*/RegInit regs, /*bitset<16>*/RegInit regs_has_pc, int cond )
{
	if ( regs_has_pc[cond][0] == 1 ) return APE_trust_2;
	//else if (  )
	
	
		 if ( regs[cond][0] == 0 ) return APE_trust_0;
	else if ( regs[cond][1] == 0 ) return APE_trust_1;
	else if ( regs[cond][2] == 0 ) return APE_trust_2;
	else if ( regs[cond][3] == 0 ) return APE_trust_3;
	else 					 return APE_trust_4;
}

bool APE_flag_check_ARM::decrypt_exist(int i, int s, int j, int mode)
{
	bool load=false, store=false;
	for (int k=s; k<=j; k++)
	{
		if ( flow_ARM->at(mode)->fl[i][k].type == LOAD) load = true;
		if ( flow_ARM->at(mode)->fl[i][k].type == STORE) store = true;
	}
	return load & store;
}

APE_flow_info_flag APE_flag_check_ARM::forward_flow( int i, int _j, int mode, /*bitset<16>*/RegInit regs, /*bitset<16>*/RegInit regs_has_pc )
{
  int offset;
  int patterns = 0;
  int disas_len = 0;
  int BX_patterns = 0;
  //int sis_calls = 0;
  int trust, st, fn;
  int j = _j;
  int cond;
  bool al;
  bool store_was = false, load_was = false;
  APE_flow_info_flag ret_vec;
  ret_vec.regs.reset(14);
  ret_vec.regs_has_pc.reset(14);
  
  for ( int l=0; l<=14; l++ )
	for ( int k=0; k<16; k++ )
	  ret_vec.regs[l][k] = regs[l][k];

  for ( int l=0; l<=14; l++ )
	for ( int k=0; k<16; k++ )
	  ret_vec.regs_has_pc[l][k] = regs_has_pc[l][k];
  
  ret_vec.regs.m = mode;
  ret_vec.regs.ALcond(cond);
  
  ret_vec.regs_has_pc.m = mode;
  ret_vec.regs_has_pc.ALcond(cond);
  
  if ( !visited[mode][i] )
  {
	visited[mode][i] = true;
  }
  else return ret_vec;
  
  int m = mode;
  
	while ( i>=0 && i < flow_ARM->at(m)->fl.size() && j>=0 && j < flow_ARM->at(m)->fl[i].size() )
	{//cout<<"___|_"<<i<<"_|___"<<endl;
		if ( flow_ARM->at(m)->fl[i][j].type != _UNDEFINED &&
			flow_ARM->at(m)->fl[i][j].type != _INCORRECT &&
			flow_ARM->at(m)->fl[i][j].ext_off == false )
		{
			ret_vec.inc_disas_len();
			//TEST
			/*
			cout<<ret_vec.regs[14]<<" | "<<ret_vec.regs_has_pc[14]<<endl;
			cout<<"i= "<<i<<" j= "<<j<<" ";
			if (flow_ARM->at(m)->fl[i][j].reg_has_pc>=0 && flow_ARM->at(m)->fl[i][j].reg_has_pc<16  )
				cout<<flow_ARM->at(m)->fl[i][j].reg_has_pc<<" | ";
			else cout<<"N/A | ";
			flow_ARM->at(m)->fl[i][j].printType(); cout<<endl<<" | ";
			flow_ARM->at(m)->fl[i][j].printInstr(); cout<<endl;
			*/ 
			//TEST
		}
	  
	    if ( flow_ARM->at(m)->fl[i][j].ext_off == false )
			switch ( flow_ARM->at(m)->fl[i][j].type )
			{
				case BRANCH_LINK_EXCHANGE: //===========================================================
					/*trust = trust_pattern( ret_vec.regs );
					ret_vec.patterns_trust += trust;
					ret_vec.patterns_num++;
					if (trust!=0) ret_vec.patterns_init++;*/
					cond = flow_ARM->at(m)->fl[i][j].condition;
					// If have a immediate value
					if ( flow_ARM->at(m)->fl[i][j].visible_branch_address )
					{
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
						if ( offset > -1 && offset < MAX_DATA_LEN )
						{
							ret_vec.getpc++;
							ret_vec.regs[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs; ret_vec.regs.ALcond(cond);
							
							int f = flow_ARM->at(!m)->offsets[ offset ].first;
							int s = flow_ARM->at(!m)->offsets[ offset ].second;
							if ( f != -1 && s != -1 ) // GOTO THUMB MODE
							{//cout<<endl<<"======================BRANCH_LINK_EXCHANGE_UPPER======================"<<endl;
								if ( /*f < i ||*/ f==i && s<=j) 
									if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
										ret_vec.cycle_number++;
								ret_vec.regs.ALcond(cond);
								ret_vec.regs_has_pc.ALcond(cond);
								
								ret_vec += forward_flow( f, s, !m, ret_vec.regs, ret_vec.regs_has_pc );
							}
						}
					}
					else
					{
						/*if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
									== flow_ARM->at(m)->fl[i][j].used_regs )
						{
							ret_vec.BX_patterns++;
							ret_vec.getpc++;
						}*/
						if ( (ret_vec.regs_has_pc.get(cond) & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
						{
							ret_vec.branch_use_pc += 1;
							offset = flow_ARM->at(m)->fl[i][j].offset;
							for ( int t=MAX_BX_GAP ; t>=0; t-- )
							{
								if (offset-t < 0) continue;
								int f = flow_ARM->at(!m)->offsets[ offset-t ].first;
								if ( (f != -1) )
								{//cout<<endl<<"======================BRANCH_LINK_EXCHANGE======================"<<endl;
									ret_vec.mode_change++;
									ret_vec.regs.ALcond(cond);
									ret_vec.regs_has_pc.ALcond(cond);
									
									ret_vec += forward_flow( f, 0, !m, ret_vec.regs, ret_vec.regs_has_pc );
									break;
								}
							}
						}
					}
					//ret_vec.regs[0] = 1;
					break;
					
				case BRANCH_EXCHANGE: //===========================================================
					// MAY CHANGE TO THUMB
					/*if ( ( ret_vec.regs & flow_ARM->at(m)->fl[i][j].used_regs ) 
									== flow_ARM->at(m)->fl[i][j].used_regs )
							ret_vec.BX_patterns++;*/
					cond = flow_ARM->at(m)->fl[i][j].condition;
					
					if ( (ret_vec.regs_has_pc.get(cond) & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
					{
						ret_vec.branch_use_pc += 1;
						offset = flow_ARM->at(m)->fl[i][j].offset;
						for ( int t=MAX_BX_GAP ; t>=0; t-- )
						{
							if (offset-t < 0) continue;
							int f = flow_ARM->at(!m)->offsets[ offset-t ].first;
							if ( (f != -1) )
							{//cout<<endl<<"======================BRANCH_EXCHANGE======================"<<endl;
								ret_vec.mode_change++;
								ret_vec.regs.ALcond(cond);
								ret_vec.regs_has_pc.ALcond(cond);
								
								ret_vec += forward_flow( f, 0, !m, ret_vec.regs, ret_vec.regs_has_pc );
								break;
							}
						}
					}
					break;
					
				case BRANCH_LINK: //===========================================================
					/*trust = trust_pattern( ret_vec.regs );
					ret_vec.patterns_trust += trust;
					ret_vec.patterns_num++;
					if (trust!=0) ret_vec.patterns_init++;*/
					cond = flow_ARM->at(m)->fl[i][j].condition;
					
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
					if ( offset > -1 && offset < MAX_DATA_LEN )
					{
						ret_vec.getpc++;
						ret_vec.regs[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs; ret_vec.regs.ALcond(cond);
						
						int f = flow_ARM->at(m)->offsets[ offset ].first;
						int s = flow_ARM->at(m)->offsets[ offset ].second;
						if ( f != -1 && s != -1 )
						{
							if ( /*f < i ||*/ f==i && s<=j) 
								if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
										ret_vec.cycle_number++;
							else 
							{//cout<<endl<<"======================BRANCH_LINK======================"<<endl;
								ret_vec += forward_flow( f, s, m, ret_vec.regs, ret_vec.regs_has_pc );
							}
						}
					}
					//ret_vec.regs[0] = 1;
					break;
					
				case BRANCH: //===========================================================
					cond = flow_ARM->at(m)->fl[i][j].condition;
					
					offset = flow_ARM->at(m)->fl[i][j].branch_offset + flow_ARM->at(m)->fl[i][j].offset;
					if ( offset > -1 && offset < MAX_DATA_LEN )
					{	
						int f = flow_ARM->at(m)->offsets[ offset ].first;
						int s = flow_ARM->at(m)->offsets[ offset ].second;
						if ( f != -1 && s != -1 )
						{
							if ( /*f < i ||*/ f==i && s<=j) 
								if ( decrypt_exist(i, s, j, m) /*load_was && store_was*/ )
										ret_vec.cycle_number++;
							else 
							{//cout<<endl<<"======================BRANCH======================"<<endl;
								ret_vec += forward_flow( f, s, m, ret_vec.regs, ret_vec.regs_has_pc );
							}
						}
					}
					break;
		
				case LOAD: //===========================================================
				case STORE: //===========================================================
					cond = flow_ARM->at(m)->fl[i][j].condition;
					st = cond*(!al) + 14*m;
					fn = cond*(!al) + 13*(al) + m;
					
					if ( flow_ARM->at(m)->fl[i][j].type == LOAD)
					{
					  al = (cond==14) ? 1 : 0;
					  for ( cond=st; cond<=fn; cond++ )
					  {
						if ( ( ret_vec.regs[cond] & flow_ARM->at(m)->fl[i][j].used_regs ) 
										== flow_ARM->at(m)->fl[i][j].used_regs )
						{
							ret_vec.regs[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs.ALcond(cond);
						}
						else
						{
							ret_vec.regs[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs.ALcond(cond);
							ret_vec.regs_has_pc[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs_has_pc.ALcond(cond);
						}
					  }
					}
					
					if ( flow_ARM->at(m)->fl[i][j].type == LOAD)
						ret_vec.load_num++;
					else
						ret_vec.store_num++;
					
					if ( (ret_vec.regs_has_pc.get(cond) & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
						ret_vec.usepc += USE_PC_RATE;
					break;
		
				case DATA_PROC: //===========================================================
				case COPROCESSOR:
				case DSP:
					cond = flow_ARM->at(m)->fl[i][j].condition;
					al = (cond==14) ? 1 : 0;
					st = cond*(!al) + 14*m;
					fn = cond*(!al) + 13*(al) + m;
					
					switch ( flow_ARM->at(m)->fl[i][j].data_instr_type )
					{
						// COMPARE
						case TST:
						case TEQ:
						case CMP:
						case CMN:
							//if ( flow_ARM->at(m)->fl[i][j].change_flags ) 
							{ ret_vec.regs.Sbit(); ret_vec.regs_has_pc.Sbit(); }
							break;
							
						// LOGICAL
						case SUB:
						case EOR:
							for ( cond=st; cond<=fn; cond++ )
							{
								if ( flow_ARM->at(m)->fl[i][j].used_regs == flow_ARM->at(m)->fl[i][j].changed_regs )
								{
									ret_vec.regs[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs.ALcond(cond);
									ret_vec.regs_has_pc[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs_has_pc.ALcond(cond);
								}
								if ( flow_ARM->at(m)->fl[i][j].change_flags ) 
								{ ret_vec.regs.Sbit(); ret_vec.regs_has_pc.Sbit(); }
							}
							break;
						
						case ORR:
						case BIC:
						case _AND:
						
						// ARIPHMETIC
						case ADD:
						case ADC:
						case SBC:
						case RSB:				
						case RSC:
						
						// MOV
						case MOV:
						case MVN:
						default:
							for ( cond=st; cond<=fn; cond++ )
							{
								if ( ( ret_vec.regs[cond] & flow_ARM->at(m)->fl[i][j].used_regs ) 
										== flow_ARM->at(m)->fl[i][j].used_regs )
								{
									ret_vec.regs[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs.ALcond(cond);
									if (flow_ARM->at(m)->fl[i][j].reg_has_pc != -1)
									{
										ret_vec.getpc++;
										ret_vec.regs_has_pc[cond][flow_ARM->at(m)->fl[i][j].reg_has_pc] = 1;
										//flow_ARM->at(m)->fl[i][j].printInstr(); cout<<" | "<<ret_vec.regs_has_pc<<endl; 
									}
									else if ( (ret_vec.regs_has_pc[cond] & flow_ARM->at(m)->fl[i][j].used_regs).count() != 0 )
									{
										ret_vec.getpc++;
										ret_vec.regs_has_pc[cond] |= flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs_has_pc.ALcond(cond);
									}
									else if ( flow_ARM->at(m)->fl[i][j].used_regs.count() != 0 )
									{
										ret_vec.regs_has_pc[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs_has_pc.ALcond(cond);
									}
								}
								else 
								{
									ret_vec.regs[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs.ALcond(cond);
									ret_vec.regs_has_pc[cond] &= ~flow_ARM->at(m)->fl[i][j].changed_regs;// ret_vec.regs_has_pc.ALcond(cond);
								}
								if ( flow_ARM->at(m)->fl[i][j].change_flags ) 
								{ ret_vec.regs.Sbit(); ret_vec.regs_has_pc.Sbit(); }
							}
							break;
					}
					break;
		
				case SVC: //===========================================================
					cond = flow_ARM->at(m)->fl[i][j].condition;
					//bool trust;
				
					al = (cond==14) ? 1 : 0;
					st = cond*(!al) + 14*m;
					fn = cond*(!al) + 13*(al) + m;
					//cout<<"_st= "<<st<<"_fn= "<<fn<<endl;
					for ( cond=st; cond<=fn; cond++ )
					{
						if ( m == 0 || m == 1 && ret_vec.regs[cond][7] == 1 )
						{
							trust = trust_pattern( ret_vec.regs, ret_vec.regs_has_pc, cond );
							ret_vec.patterns_trust += trust;	
							if (trust!=0) ret_vec.svc_num++;
							// TEST
							
							if (trust != 0)
							{cout<<trust<<endl;
								cout<<ret_vec.regs[cond]<<" | "<<ret_vec.regs_has_pc[cond]<<" | cond= "<<cond<<endl;
								for(int j=0; j<flow_ARM->at(m)->fl[i].size(); j++)
									if ( flow_ARM->at(m)->fl[i][j].ext_off != true )
									{
										flow_ARM->at(m)->fl[i][j].printType();cout<<" || ";
										flow_ARM->at(m)->fl[i][j].printInstr();cout<<endl;
									}
							}
							// TEST
							ret_vec.regs.reset(cond);
							//ret_vec.regs[0] = ret_vec.regs[13] = ret_vec.regs[15] = 1;
						}
					}
					ret_vec.patterns_num++;
					break;
		
				case _UNDEFINED: //===========================================================
				case _INCORRECT:
				default:
					//ret_vec.dec_disas_len();
					//if (flow_ARM->at(m)->ext_offset[ flow_ARM->at(m)->fl[i][j].offset ] == false)
					if ( flow_ARM->at(m)->fl[i][j].ext_off == false )
						return ret_vec;
					break;
			}
		
		//cout<<"|_"<<i<<"_|_"<<j<<"___|_size= "<<flow_ARM->at(m)->fl.size()<<" | jsize= "<<flow_ARM->at(m)->fl[i].size()/*<<endl*/;
		//cout<<"_|__"; flow_ARM->at(m)->fl[i][j].printOffset(); cout<<endl;
		
		if ( flow_ARM->at(m)->fl[i][j].ext_off == true )
		{
			offset = flow_ARM->at(m)->fl[i][j].offset;
			
			//ret_vec.disas_len--;cout<<endl<<"======================EXT_GOOOO======================"<<endl;
			ret_vec += forward_flow( flow_ARM->at(m)->offsets[ offset ].first, 
									 flow_ARM->at(m)->offsets[ offset ].second, 
									 m, 
									 ret_vec.regs,
									 ret_vec.regs_has_pc 
									 );
									 
			return ret_vec;
		}
		else j++;
	}
  
  return ret_vec;
}

//========================DisasLength APE CLASSIFIER===================================//
DisasLength_APE_ARM::DisasLength_APE_ARM()
{
	fn = 0.05;
	fp_r = 0.67;
	fp_m = 0.95;
	complexity = 12;
}

bool DisasLength_APE_ARM::check()
{
	return APE_classifier->flow_info.disas_len;
}

//========================Patterns APE CLASSIFIER===================================//
Call_patterns_init_APE_ARM::Call_patterns_init_APE_ARM()
{
	fn = 0.05;
	fp_r = 0.67;
	fp_m = 0.95;
	complexity = 12;
}

bool Call_patterns_init_APE_ARM::check()
{
	return APE_classifier->flow_info.patterns;
}

//========================BX patterns APE CLASSIFIER===================================//
BX_patterns_init_APE_ARM::BX_patterns_init_APE_ARM()
{
	fn = 0.05;
	fp_r = 0.67;
	fp_m = 0.95;
	complexity = 12;
}

bool BX_patterns_init_APE_ARM::check()
{
	return APE_classifier->flow_info.BX_num;
}

//========================Cycle init APE CLASSIFIER===================================//
Decrypt_init_APE_ARM::Decrypt_init_APE_ARM()
{
	
}

bool Decrypt_init_APE_ARM::check()
{
	return APE_classifier->flow_info.decrypt_exist;
}


//========================GET USE PC APE CLASSIFIER===================================//
Get_Use_PC_APE_ARM::Get_Use_PC_APE_ARM()
{

}
	
bool Get_Use_PC_APE_ARM::check()
{
	return APE_classifier->flow_info.getpc;
}

//========================MODE CHANGE APE CLASSIFIER===================================//
Mode_change_APE_ARM::Mode_change_APE_ARM()
{

}
	
bool Mode_change_APE_ARM::check()
{
	return APE_classifier->flow_info.mode_change;
}

//========================STRIDE ARM CLASSIFIER===================================//
Stride_ARM::Stride_ARM()
{
	min_nop_len = MIN_NOP_LEN;
}

bool Stride_ARM::check() 
{ 
	return Stride_check(true);
}

bool Stride_ARM::Stride_check(bool instr)
{
	bool ARM_nop = false, THUMB_nop = false, some_nop = false;
	int max_mode, min_len, max_len;
	/*
	if ( visited_off.size() != 0 )
	{
		if ( visited_off[0] != NULL )
			delete [] visited_off[0];
		if ( visited_off[1] != NULL )
			delete [] visited_off[1];
	}
	*/
	visited_off.push_back( new bool[MAX_DATA_LEN] );
	visited_off.push_back( new bool[MAX_DATA_LEN] );
	
	for (int i=0; i<MAX_DATA_LEN; i++)
	{
		visited_off[0][i] = false;
		visited_off[1][i] = false;
	}
	
	for (int i=0; i<MAX_DATA_LEN-min_nop_len; i++)
	{
		ARM_nop = find_sled(i, min_nop_len, ARM_MODE, instr);
		THUMB_nop = find_sled(i, min_nop_len, THUMB_MODE, instr);
		if ( ARM_nop || THUMB_nop )
			return true;
	}
	
	delete [] visited_off[0];
	delete [] visited_off[1];
	visited_off.clear();
	
	return false;
}

bool Stride_ARM::find_sled( int offset, int len, int mode, bool instr )
{
	int i,j;
	int instr_size = 2+2*(!mode); // 2+2*(!mode) == 4 in ARM and == 2 in THUMB
	if (offset+len >= MAX_DATA_LEN) return false;
	
	if (instr)
	{
		for ( i = 0; i < len; i+= 2+2*(!mode) )
		{
			//if ( !visited_off[mode][i+offset] )
				if (!is_valid_sequence( i+offset, len-i, mode ) )
					return false;
		}
		return true;
	}/*
	else
	{
		for ( j = 0; j < 2+2*(!mode); j++ )
				for ( i = j; i < len; i+= 2+2*(!mode) )
					if (!is_valid_sequence( i, len-i, mode ) )
						return false;
		return true;
	}*/
}

bool Stride_ARM::is_valid_sequence( int offset, int len, int mode ) 
{	
	bool jump_exist;
	//visited_off[mode][offset] = true;
	
	int i = flow_ARM->at(mode)->offsets[offset].first;
	int j = flow_ARM->at(mode)->offsets[offset].second;
	if ( i == -1 ) return false;
	
	int size = flow_ARM->at(mode)->fl[i].size();
	int last_offset = flow_ARM->at(mode)->fl[i][size-1].offset;
	int last_len = size - j;
	int ext = flow_ARM->at(mode)->ext_offset[last_offset] == true;
	
	if ( last_len >= len )
		return true;
	else
	{
		for( int off=j; off<=size-1; off++ )
		{
			int type = flow_ARM->at(mode)->fl[i][size-1].type;
			if ( !(type == BRANCH_LINK_EXCHANGE ||
				  type == BRANCH_EXCHANGE ||
				  type == BRANCH_LINK ||
				  type == BRANCH)
				  ) ;
			jump_exist = false;
		}
	}
	
	if ( jump_exist )
		return true;
	else
	{
		if (ext)
		{
			return is_valid_sequence( last_offset, len-last_len, mode );
		}
		else
			return false;
	}
}

//========================STRIDE per instruction CLASSIFIER===================================//
/*Stride_ARM_instr::Stride_ARM_instr()
{
	
}
bool Stride_ARM_instr::check() 
{ 
	return Stride_check(true);
}

//========================STRIDE per byte CLASSIFIER===================================//
Stride_ARM_byte::Stride_ARM_byte()
{
	
}
bool Stride_ARM_byte::check() 
{ 
	return Stride_check(false);
}*/


//---------------------------------------------------------------------------------------------------//
//=====================================DINAMIC PARENT CLASS==========================================//
EmulBased_ARM::EmulBased_ARM()
{
	wxNum = unReadNum = unWriteNum
	 = branchWrited = modeChanged
	 = malicCall = unReadWriteNum = false;
}

bool EmulBased_ARM::Emul_check(const unsigned char* _buffer, int data_len)
{
	bool check_success = false;
	long for_max_val;
	
	wxNum = unReadNum = unWriteNum
	= branchWrited = modeChanged = false;
	
	for ( int m=0; m<=1; m++)
	{
		/*
		if ( offsets_APE[m].size() == 0 ) for_max_val = data_len-4;
		else 
			for_max_val = offsets_APE[m].size();
		*/
		//for (int i=0; i<data_len-4 && i<200; i++)
	  if ( offsets_APE[m].size() != 0 )
		for ( int i = 0; i < offsets_APE[m].size(); i++ )
		{//cout<<"mode= "<<m<<" , offset= "<<i<<endl;
			check_success = false;
			
			emul.execute(_buffer,data_len,m,/*0*/ offsets_APE[m][i].first);
			/* not used in demorpheus
			if (emul.modeChanged)
				modeChanged = check_success = true;
			*/
			
			/*
			if (emul.unReadNum >= MAX_unReadNum)
				unReadNum = true;
			
			if (emul.unWriteNum >= MAX_unWriteNum)
				unWriteNum = true;
			*/
			//if ( offsets_APE[m][i].second && 1 )
			{
				if ( emul.unWriteNum >= MAX_unWriteNum && emul.unReadNum >= MAX_unReadNum )
					unReadWriteNum = check_success = true;
				
				if (emul.branchWrited)
					branchWrited = check_success = true;
				
				if (emul.wxNum >= MAX_wxNum)
					wxNum = check_success = true;
			}
			
			//if ( offsets_APE[m][i].second && 2)
			{
				if (emul.malicCall)
					malicCall = check_success = true;
			}
			
			if (check_success) return true;
		}
	}
	
	return false;
}
/*
	bool wxNum, unReadNum, unWriteNum;
	bool branchWrited;
	bool modeChanged;
*/





//=====================================wxNum DINAMIC CLASS==========================================//
wxNum_EmulBased_ARM::wxNum_EmulBased_ARM()
{

}
	
bool wxNum_EmulBased_ARM::check()
{
	return emul_ARM->wxNum;
}

//=====================================unReadNum DINAMIC CLASS==========================================//
decrypt_EmulBased_ARM::decrypt_EmulBased_ARM()
{

}
	
bool decrypt_EmulBased_ARM::check()
{
	return emul_ARM->unReadWriteNum & emul_ARM->branchWrited;
}

//=====================================unWriteNum DINAMIC CLASS==========================================//
/*unWriteNum_EmulBased_ARM::unWriteNum_EmulBased_ARM()
{

}
	
bool unWriteNum_EmulBased_ARM::check()
{
	return emul_ARM->unWriteNum;
}*/

//=====================================branchWrited DINAMIC CLASS==========================================//
branchWrited_EmulBased_ARM::branchWrited_EmulBased_ARM()
{

}

bool branchWrited_EmulBased_ARM::check()
{
	return emul_ARM->branchWrited;
}

//=====================================modeChanged DINAMIC CLASS==========================================//
modeChanged_EmulBased_ARM::modeChanged_EmulBased_ARM()
{

}
	
bool modeChanged_EmulBased_ARM::check()
{
	return emul_ARM->modeChanged;
}

//=====================================malicCall DINAMIC CLASS==========================================//
malicCall_EmulBased_ARM::malicCall_EmulBased_ARM()
{
	
}
	
bool malicCall_EmulBased_ARM::check()
{
	return emul_ARM->malicCall;
}
	















