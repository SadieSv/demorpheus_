#include <iostream>
#include <sstream>
#include <stdio.h>
#include <sys/time.h>
#include <pcap.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include <arpa/inet.h>

#include "macros.h"
#include "graphTopology.h"

using namespace std;

unsigned char * payload;
static int size_payload = 0;
static char * ip_saddr;
static char * ip_daddr;
static uint tcp_source;
static uint tcp_dest;
static struct iphdr ip_hdr;
static struct tcphdr * tcp_hdr;


string ts(){
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	char * cur_time = asctime(timeinfo);
	cur_time[strlen(cur_time)-1] = '\0';
	return cur_time;
};


#define PRINT_IP_FROM ip_saddr << ":" << tcp_source
#define PRINT_IP_TO ip_daddr << ":" << tcp_dest
#define PRINT_LOG if(LOG) cout << " [Demorpheus] ["<<ts()<<"] Shellcode found in traffic from " << PRINT_IP_FROM << " to " << PRINT_IP_TO 
//<< " shellcode types : "  

static struct option long_options[] = 
{
	{"error-rate", required_argument, 0, 'e'},
	{"topology", required_argument, 0, 't'},
	{"interface", required_argument, 0, 'i'},
	{"file", required_argument, 0, 'f'},
	{"help", no_argument, 0, 'h'},
	{"mode", no_argument, 0, 'm'}
};

void printHelp()
{
	cout << "usage: ./demorpheus OPTION [file name or interface] [--error-rate fp|fn] [--topology linear|hybrid] [--mode x86|arm]\n" <<
			"OPTIONS: \n"<< 
			"-i \t\t interface\n"<<
			"-f \t\t file\n"<<
			"-h \t\t help message\n\n" << endl;
	cout << "error-rate: \n fp calculates false positives rate \n fn calculates false negatives rate" << endl;
	cout << " topology is hybrid by default" << endl << endl;
}

bool process_tcp_pkt( const u_char * raw_tcp_part, int remaining_len, struct iphdr * hdr_ip, int size_ip )
{
	//PRINT_DEBUG << "tcp packet, remaining len: " <<remaining_len << endl;
	if( remaining_len < (int)sizeof(struct tcphdr ) ) {
		//PRINT_DEBUG << "partial_fill_chunk_tcp: remaining_len < sizeof(struct tcphdr )" << endl;
		return false;
	}

	struct tcphdr * hdr_tcp = (struct tcphdr * )( raw_tcp_part );
	int size_tcp = hdr_tcp->doff * 4;
	
	//PRINT_DEBUG << "size_tcp: " <<size_tcp << endl;
	
	if ( size_tcp < 20 ) {
		//PRINT_DEBUG << "partial_fill_chunk_tcp: Invalid TCP header length: " << size_tcp << " bytes, less than 20 bytes" << endl;
		return false;
	}

	tcp_hdr = hdr_tcp;
	ip_saddr = inet_ntoa(*(struct in_addr * )&hdr_ip->saddr);
	ip_daddr = inet_ntoa(*(struct in_addr * )&hdr_ip->daddr);
	tcp_source = ntohs(hdr_tcp->source);
	tcp_dest = ntohs(hdr_tcp->dest);
	
	payload = (unsigned char *)(raw_tcp_part /*+ size_tcp*/);
	size_payload = remaining_len /*- size_tcp*/;
	
	//PRINT_DEBUG << "real payload size: " << strlen((const char *)payload) << endl;
	//PRINT_DEBUG << "now the real payload size: " << remaining_len - size_tcp  << endl;
	//PRINT_DEBUG << "payload: " << payload << endl << "raw_tcp_part: " << (unsigned char*)(raw_tcp_part+size_tcp)<< endl;
	//PRINT_DEBUG << "raw_tcp_part: " << (unsigned char*)(raw_tcp_part) << endl;

	return true;
}

bool process_udp_pkt( const u_char * raw_udp_part, int remaining_len, struct iphdr * hdr_ip, int size_ip  )
{
	if( remaining_len < (int)sizeof(struct udphdr ) ) {
		//PRINT_DEBUG << "partial_fill_chunk_udp: remaining_len < sizeof(struct tcphdr )" << endl;
		return false;
	}

	struct udphdr * hdr_udp = (struct udphdr * )( raw_udp_part );
	int size_udp = sizeof(udphdr);

	return true;
}


bool process_ethernet_pkt( pcap_pkthdr & header, const u_char * raw_packet )
{
	int caplen = header.caplen; /* length of portion present from bpf  */
	int length = header.len;    /* length of this packet off the wire  */

	//PRINT_DEBUG << "captured eth packet, caplen: "<<caplen << " len: "<<length<<endl;

	if( caplen < length ) {
		//PRINT_DEBUG << "init_packet: caplen " << caplen << " is less than packet size " << length << endl;
		return false;
	}

	if( caplen < 14 ) { 
		//PRINT_DEBUG << "init_packet: caplen " << caplen << " is less than header size, total packet size " << length << endl;
		return false;
	}

	struct ether_header * hdr_ethernet = (struct ether_header*)( raw_packet );
	u_int16_t type = ntohs( hdr_ethernet->ether_type );
 
	if( type != ETHERTYPE_IP ) {
		//PRINT_DEBUG << "got packet with non-IPv4 header , type = " << type << endl;
		return false;
	}

	struct iphdr * hdr_ip = (struct iphdr * )(raw_packet + sizeof(struct ether_header) );
	int size_ip = hdr_ip->ihl * 4;
	//PRINT_DEBUG << "ip header len: " << size_ip << endl;

	if (size_ip < 20) {
		//PRINT_DEBUG << "init_packet: Invalid IP header length: " << size_ip << " bytes, less than 20 bytes" << endl;
		return false;
	}

	//tcp packet has been received
	if( hdr_ip->protocol == 6 ) {
		return process_tcp_pkt( raw_packet + sizeof(ether_header) + size_ip, length - sizeof(ether_header) + size_ip, hdr_ip, size_ip );
		/*payload = (unsigned char *)(raw_packet + sizeof(ether_header) + size_ip);
		size_payload = length - sizeof(ether_header) + size_ip;
		return 0;*/		
	}
	//udp packet
	else if( hdr_ip->protocol == 17 ) {
		return process_udp_pkt( raw_packet + sizeof(ether_header) + size_ip, length - sizeof(ether_header) + size_ip, hdr_ip, size_ip );
		/*payload = (unsigned char *)(raw_packet + sizeof(ether_header) + size_ip);
		size_payload = length - sizeof(ether_header) + size_ip;
		return 0;*/
	}

	return false;
}



bool process_raw_pkt( pcap_pkthdr & header, const u_char * raw_packet )
{
	size_t caplen = header.caplen; /* length of portion present from bpf  */
	size_t length = header.len;    /* length of this packet off the wire  */

	if( caplen < length ) {
		//PRINT_DEBUG << "init_packet: caplen " << caplen << " is less than packet size " << length << endl;
		return false;
	}

	struct iphdr * hdr_ip = (struct iphdr * )(raw_packet);
	int size_ip = hdr_ip->ihl * 4;

	if (size_ip < 20) {
		//PRINT_DEBUG << "init_packet: Invalid IP header length: " << size_ip << " bytes, less than 20 bytes" << endl;
		return false;
	}

	if( hdr_ip->protocol == 6 ) {
		return process_tcp_pkt( raw_packet + size_ip, length - size_ip, hdr_ip, size_ip );
	}
	/*else if( hdr_ip->protocol == 17 ) {
		return process_udp_pkt( raw_packet + size_ip, length - size_ip, hdr_ip, size_ip );
	}*/
 
	return false;
}

int main( int argc, char **argv)
{
	FILE* pFile = NULL;
	pcap_t *handle = NULL;
	unsigned char buffer[MAX_DATA_LEN];
 	char errbuf[PCAP_ERRBUF_SIZE];
	Classifier *cl;
	GraphTopology *topology;
	struct timeval lastTime, totalTime;
	string dev, file_name;
	bool fp = false, fn = false, hybrid = true, interface = false, file = false, arm = false;
	extern int optind, opterr, optopt;
	extern char *optarg;
	
	int opt;
	
	while (1) {
		int option_index = 0;
		opt = getopt_long(argc, argv, "i:f:e:t:h:m:", long_options, &option_index);
		
		if(opt == -1) break;
		
		switch (opt) {
			case 'i':
				dev = optarg;
				interface = true;
				break;
			case 'f':
				file = true;
				file_name = optarg;
				pFile = fopen( file_name.c_str(), "rb" );
				if ( pFile == NULL ) {
					PRINT_ERROR(" Cannot open file with binary data " );
				}
				break;
			case 'e':
				if( !strcmp(optarg, "fp") ) fp = true;
				else if( !strcmp(optarg, "fn")) fn = true;
				break;
			case 't':
				if( !strcmp(optarg, "hybrid")) hybrid = true;
				else if( !strcmp(optarg, "linear")) hybrid = false;
				break;
			case 'h':
				printHelp();
				return 0;
				break;
			case 'm':
				if( !strcmp(optarg, "x86")) arm = false;
				else if( !strcmp(optarg, "arm")) arm = true;
				break;
			default:
				abort();
		}
	}
	
	if(!interface && !file) {
		PRINT_ERROR("No interface or filename is given");
		return 1;
	}
	else if(interface && file) {
		PRINT_ERROR( " Interface and file options are incompatible");
		return 1;
	}
	
	//total number of processed pieces of data and total number of detected shellcodes
	int total_cnt = 0;
	int s_cnt = 0;
	totalTime.tv_sec = 0;
	totalTime.tv_usec = 0;
	
	topology = new GraphTopology();
	
	if (!arm) // x86
	{
		//set of elementary classifiers added to topology. 
		//comment those you don't want to use
		
		topology->addClassifier(new DisasLength());
		topology->addClassifier(new PushCall());
		topology->addClassifier(new DataFlowAnomaly());
		topology->addClassifier(new DisasOffset());
		topology->addClassifier(new RaceWalk());
		topology->addClassifier(new HDD(0));
		topology->addClassifier(new CycleFinder());

		//topology->recognizeShellcodeClasses();
		
		//select Hybrid or Linear topology
		if(hybrid) topology->makeHybridTopology();
		else topology->makeLinearTopology();
		
		topology->printTopology();
	}
	else // ARM
	{
		// STATIC
		//topology->addStaticClassifier_ARM( new DisasLength_ARM() );
		//topology->addStaticClassifier_ARM( new DisasLength_APE_ARM() );
		topology->addStaticClassifier_ARM( new Call_patterns_init_APE_ARM() );
		//topology->addStaticClassifier_ARM( new APE_flag_check_ARM() );
		topology->addStaticClassifier_ARM( new Decrypt_init_APE_ARM() );
		topology->addStaticClassifier_ARM( new Mode_change_APE_ARM() );
		
		//topology->addStaticClassifier_ARM( new Stride_ARM() );
		
		// DYNAMIC
		topology->addDynamicClassifier_ARM( new wxNum_EmulBased_ARM() );
		topology->addDynamicClassifier_ARM( new decrypt_EmulBased_ARM() );
		topology->addDynamicClassifier_ARM( new branchWrited_EmulBased_ARM() );
		//topology->addDynamicClassifier_ARM( new modeChanged_EmulBased_ARM() );
		topology->addDynamicClassifier_ARM( new malicCall_EmulBased_ARM() );
	}
	
	//catch the packets, retrieve payload and process it
	if (interface) {
		handle = pcap_open_live(dev.c_str(), BUFSIZ, 1, 1000, errbuf);
		if (handle == NULL ) {
			PRINT_ERROR( " Cannot open interface");
		}
		
		int index = 0;
		while(1) {
			pcap_pkthdr header;
			const u_char * packet = pcap_next( handle, &header );
			if( !packet )
				continue;

			if( pcap_datalink(handle) == DLT_EN10MB ) {
				process_ethernet_pkt( header, packet);
			}
			else if( pcap_datalink(handle) == DLT_RAW ) 
				process_raw_pkt( header, packet );
			
			//if ( size_payload <= MIN_DATA_LEN) continue;
			
			struct timeval current_time;
			while (size_payload - index > 0) {
				int cnt = 0;
				if ( size_payload - index < MAX_DATA_LEN )
					cnt = size_payload - index;
				else cnt = MAX_DATA_LEN;
				
				PRINT_DEBUG << "number of read bytes: " << cnt << endl;
				memcpy(buffer, payload /*+ index*sizeof(unsigned char)+1*/, cnt); 
				buffer[cnt]='\0';
				index+=cnt;
				//PRINT_DEBUG << "read payload: " << buffer << endl;
				//PRINT_DEBUG << "read real payload: " << payload + index << endl;
				//PRINT_DEBUG << "offset = " << index << endl;
				
				if( topology->executeTopology(buffer, cnt, hybrid, arm) ) {
					PRINT_LOG << topology->printClasses() << endl;
					//cout<<endl<<" There was found a SHELLCODE!"<<endl<<endl;
					s_cnt++;
				}
				total_cnt++;	
			}
			index = 0;
			size_payload = 0;
			payload = NULL;
		}
		pcap_close(handle);
	}
	
	if (pFile){
		while(!feof(pFile)){
			int cnt = fread( buffer, sizeof( unsigned char ), MAX_DATA_LEN*sizeof( unsigned char ), pFile );
			if ( cnt == 0 ) continue;
			//if ( cnt < MIN_DATA_LEN ) continue;
			if ( cnt < MAX_DATA_LEN ) buffer[cnt]='\0';
			
			//PRINT_DEBUG << " number of read bytes: " << cnt << endl;
			//PRINT_DEBUG << "read payload: " << buffer << endl;
			
			if( topology->executeTopology(buffer, cnt, hybrid, arm) )  {
				s_cnt++;
				/*if (LOG)*/ cout << "[Demorpheus] [" << ts() << "] Shellcode found in file "<< file_name.c_str() << "classes: " << topology->printClasses() << endl; 
			}
			total_cnt++;
			lastTime = topology->getLastTime();
			timeval_addition(&totalTime, &totalTime, &lastTime);
		}
		
		fclose(pFile);
	}
	
	delete topology;

	//PRINT_DEBUG << total_cnt<< " " << s_cnt << std::endl;
	//PRINT_DEBUG << totalTime.tv_sec << " " << totalTime.tv_usec <<std::endl;

	return 0;	
}