#ifndef _GRAPH_TOPOLOGY_H_
#define _GRAPH_TOPOLOGY_H_

#include <iostream>
#include <vector>
#include <set>
#include <sys/time.h>

#include "classifier.h"
#include "classifier_arm.h"

//TODO: add more shellcode classes
//TODO: make topology_ private
//TODO: make mClassifier private



/**
 * Subtract the timeval values x and y, storing the result in result param
 * @param result	-	here the value of difference will be stored
 * @param x			-	parameter for subtraction
 * @param y			-	difference
 * @return	1 if the difference is negative
 * 			0 otherwise
 */
int timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y);
int timeval_addition(struct timeval *result, struct timeval *x, struct timeval *y);

double defaultQuality(double fp, double fn, double complexity);


/**
 * Struct for elementary classifier representation inside graph.
 */
struct ClassifierVertex
{
	Classifier *cl; //!< Current classifier.
	std::vector<Classifier*> outcomings; //!< List of classifiers those have edge from current.
};

class GraphTopology {
private:	
	//bool detected_classes_[SHELLCODE_TYPES];//!< Indicates classes detected by input classifiers
	struct timeval last_time_;
	vector<Classifier*> elem_classifiers_;//!< List of input classifiers
	
	vector<Classifier*> static_classifiers_ARM;
	vector<Classifier*> dynamic_classifiers_ARM;
public:
		
	/**
	 * describes topology of a graph. 
	 * Schema of topology: key is a ref to the classifier,
	 * value is a vector of classifiers - stands for classifiers which should be executed after
	 * the current
	 */
	std::map<Classifier*, std::vector<Classifier* > > topology_;	
	
//member functions
public:
	/**
	 * Constructor function.
	 * Automatically adds InitialLength classifier to topology.
	 */
	GraphTopology();
	~GraphTopology();
	
	/**
	 * adds new classifier to the whole list of available classifiers
	 */
	void addClassifier(Classifier*);
	
	void addStaticClassifier_ARM(Classifier* classifier);
	void addDynamicClassifier_ARM(Classifier* classifier);
	
	/**
	 * Function automatically makes optimal classifier topology
	 */
	void makeHybridTopology();
	void makeHybridTopologyARM();
	
	/**
	 * Makes linear combination of input classifiers.
	 * Fills topology_ with elem_classifiers_ elements.
	 */
	void makeLinearTopology();
	
	bool executeTopology_ARM();
	bool executeTopology( const unsigned char* buffer, int length, bool reduce = false, bool arm = false);
	struct timeval getLastTime() const { return last_time_; };

	void printTopology();
	string printClasses();
	
	Classifier *mClassifier; //!< Initial vertex
	
private:
	
	/**
	 * sets all elements of given array to false
	 * @param detected bool* - array of detected classes which are neede to be cleared
	 */
	void clearDetectedClasses(char&);
	
	/**
	* Recognizes shellcode classes which can be detected by input elementary classifiers.
	*/	
	void recognizeShellcodeClasses(const vector<Classifier*> classifiers, char & detected);
	
	/**
	 * Checks if given classifier detects the same classes as it specified in detected
	 * @param classifier  - given classifier to be analysed
	 * @param detected - array which contains detected classes
	 * @return true if types sets are the same
	 */
	bool isSameClasses(const Classifier* classifier, char detected);
	
	/**
	 * fills new array with shellcode types which are left to detect after given classifier
	 * will be removed from classifiers input sets
	 * @param classifier - 	classifier whose detected classes should be removed from entire
	 * 			set
	 * @param detected - 	shellcode types which are detected by entire set
	 * @param new_detected 	shellcode types which are detected by entire set without given
	 * 			classifier
	 */
	void classesLeftToRecognize(const Classifier* classifier, char detected, char& new_detected);
	
	/**
	 * makes all possible combinations from input classifiers which cover detectable shellcode classes
	 * @param classifiers	the set of classifiers from which combinations will be constructed
	 * @param detected	classes detected by input classifier sets
	 * @param level		combinations
	 */
	void makeCombinations(const vector<Classifier*> classifiers, char detected, 
				     std::vector< vector<Classifier*> > &level);
	
	//TODO: now it's done for random data set. You should consider multimedia as well
	/**
	 * calculated quality of a given layer with given measurement function
	 * @param level		the set of classifiers for which total quality will be calculated
	 * @param quality	measurement function
	 */
	double levelQuality(const vector<Classifier*> level, double (*quality)(double, double, double) = &defaultQuality);
	
	/**
	 * select level which is optimal it terms of fp, fn and complexity
	 * @param levels	combinations from which function select optimal
	 * @return vector<Classifier*> which contains optimal combination
	 */
	vector<Classifier*> selectOptimalLevel(const vector< vector<Classifier*> > &levels);
	
	/**
	 * checks is two classifiers detects same types of shellcodes (at least one)
	 * @param first_classifier
	 * @param second_classifier
	 * @return true if two classifiers have at leat one common shellcode type
	 */
	bool isOverlapped(const Classifier* first_classifier, const Classifier* second_classifier);
	
	/**
	 * links previous level of topology to the new constructed. Levels are linked due to the classes
	 * they are able to detect.
	 * @param previous	set of classifiers from previous level of topology
	 * @param current	set of classifier from new constructed level
	 */
	void linkLevels(const vector<Classifier*> previous, const vector<Classifier*> current);
	
	/**
	 * removes classifiers of level from enrite set of available classifiers
	 * @param classifiers	entire set of available classifiers
	 * @param level		set of classifiers of a new consructed level
	 */
	void updateClassifiers(vector<Classifier*> &classifiers, const vector<Classifier*> level);
		
	std::map<Classifier*, bool > markedClassifier;
	void clearMarkedClassifier();	
};

#endif