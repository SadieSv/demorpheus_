project(tests)

find_package (Boost COMPONENTS unit_test_framework REQUIRED)

set(test1_source eifg_test.cpp)
add_executable(test1 ${test1_source})
target_link_libraries(test1 ${Boost_LIBRARIES} SigFree)

add_test(NAME eifgTest COMMAND test1)