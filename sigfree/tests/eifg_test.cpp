#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE EIFG

#include "state.h"
#include "eifg.h"
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <sys/time.h>
#include <pcap.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <string>

BOOST_AUTO_TEST_SUITE(EIFGTests)

BOOST_AUTO_TEST_CASE(eifgTest)
{
    unsigned char buffer[256];
    State* s = new State();
    EIFG* ifg = new EIFG(s);
    FILE* pFile = NULL;
    string filename  = "linux_adduser";
    int i = 0;
    
    pFile = fopen( filename.c_str(), "rb" );
    if ( pFile == NULL ) {
      PRINT_ERROR(" Cannot open file with binary data " );
    }
    else
    {
	int cnt = fread( buffer, sizeof( unsigned char ), 256*sizeof( unsigned char ), pFile );
	if (cnt > 0 && cnt < 256) buffer[cnt]='\0';
	
	ifg->makeGraph(buffer, cnt, 0);
	i = ifg->getVertexNumber();
    }
    
    delete s;
    delete ifg;
    
    BOOST_CHECK_EQUAL(i, 250);
}


BOOST_AUTO_TEST_SUITE_END()
