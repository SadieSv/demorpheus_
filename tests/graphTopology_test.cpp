#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE GRAPH

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <sys/time.h>
#include <pcap.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <string>

#include "graphTopology.h"


BOOST_AUTO_TEST_SUITE(GRAPHTests)

BOOST_AUTO_TEST_CASE(linearTopologyTest)
{
    GraphTopology *topology;
    std::map<Classifier*, std::vector<Classifier*> >::iterator ptr;
    bool flag = false;
    
    topology = new GraphTopology();
    topology->addClassifier(new DisasLength());
    topology->addClassifier(new PushCall());
    topology->addClassifier(new DataFlowAnomaly());
    topology->addClassifier(new DisasOffset());
    topology->addClassifier(new RaceWalk());
    topology->addClassifier(new HDD(0));
    topology->addClassifier(new CycleFinder());
    
    topology->makeLinearTopology();
    
    ptr = topology->topology_.begin();
    if(topology->topology_.size() != 7) flag = false;
    if ((*ptr).first->info == "InitialLength"
	&& (*(++ptr)).first->info == "DisasLength"  
	&& (*(++ptr)).first->info == "PushCall" 
	&& (*(++ptr)).first->info == "DataFlowAnomaly" 
	&& (*(++ptr)).first->info == "DisasOffset"  
	&& (*(++ptr)).first->info == "RaceWalk"  
	&& (*(++ptr)).first->info == "HDD"
      
    ) flag = true;
    
    BOOST_CHECK_EQUAL(flag, true);
    delete topology;
}


/*BOOST_AUTO_TEST_CASE(shellcodeClassesTest)
{
  bool detected[SHELLCODE_TYPES];
  vector<Classifier*> classifiers;
  
  GraphTopology * topology = new GraphTopology();
  
  classifiers.push_back(new RaceWalk());
  classifiers.push_back(new CycleFinder());// = topology->elem_classifiers_;
  topology->recognizeShellcodeClasses(classifiers, detected);
  
  bool flag = false;
  if(!detected[0] 
    && detected[1] 
    && detected[2] 
  )
    flag = true;
  
  BOOST_CHECK_EQUAL(flag, true);
  delete topology;
} */

BOOST_AUTO_TEST_CASE(hybridTopology)
{
    GraphTopology* topology = new GraphTopology();
    std::vector<Classifier*> classifiers;
    char detected = 0;
    std::vector< vector<Classifier*> > level; 
    
    topology->addClassifier(new RaceWalk());
    topology->addClassifier(new CycleFinder());
    topology->addClassifier(new HDD(0));
    topology->addClassifier(new DisasOffset());
    
    topology->makeHybridTopology();
    
    bool flag = false;
    
    map<Classifier*, vector<Classifier*> >::iterator ptr = topology->topology_.find(topology->mClassifier);
    if ((*ptr).second[0]->info == "DisasOffset" && 
	(*ptr).second[1]->info == "CycleFinder"
    )
    {
	map<Classifier*, vector<Classifier*> >::iterator ptr_1 = topology->topology_.find((*ptr).second[0]);
	map<Classifier*, vector<Classifier*> >::iterator ptr_2 = topology->topology_.find((*ptr).second[1]);
	
	if ((*ptr_1).second[0]->info == "RaceWalk" && (*ptr_2).second[0]->info == "HDD" )
	  flag = true;
	
    }
    BOOST_CHECK_EQUAL(flag, true);
    delete topology;
}


BOOST_AUTO_TEST_SUITE_END()

