#include <iostream>
#include <vector>
#include <set>
#include <sys/time.h>
#include <getopt.h>
#include <string>

#include "classifier.h"
#include "macros.h"

string ts(){
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	char * cur_time = asctime(timeinfo);
	cur_time[strlen(cur_time)-1] = '\0';
	return cur_time;
};

int timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y)
{
	// Perform the carry for the later subtraction by updating y. 
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }
     
    // Compute the time remaining to wait. tv_usec is certainly positive.
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;
     
    // Return 1 if result is negative.
    return x->tv_sec < y->tv_sec;
}

int timeval_addition(struct timeval *result, struct timeval *x, struct timeval *y)
{
	result->tv_sec = x->tv_sec + y->tv_sec;
	result->tv_usec = x->tv_usec + y->tv_usec;
	if (result->tv_usec > 1000000){
		result->tv_sec++;
		result->tv_usec = result->tv_usec-1000000;
	}
	return 0;
}

static struct option long_options[] = 
{
	{"error-rate", required_argument, 0, 'e'},
	{"file", required_argument, 0, 'f'},
	{"classifier-name", required_argument, 0, 'n'},
    {"malicious-file", required_argument, 0, 'm'},
	{"help", no_argument, 0, 'h'}
};

void printHelp()
{
	cout << "usage: ./testing -f FILENAME -n CLASSIFIERNAME --error-rate [fp|fn] -h HELP\n" << endl;
}

int main(int argc, char **argv)
{
	Classifier *current;
	int length;
	FILE* pFile = NULL;
    FILE* mFile = NULL;
	string classifier_name, file_name, mal_file;
	bool fp = false;
	bool fn = false;
	unsigned char buffer[MAX_DATA_LEN];
	struct timeval startTime, endTime, diff, totalTime;
	
	totalTime.tv_sec = 0;
	totalTime.tv_usec = 0;
	
	int opt;
  
	while ( 1 ) 
	{
		int option_index = 0;
		opt = getopt_long(argc, argv, "n:f:e:h", long_options, &option_index);
		
		if(opt == -1) break;
		
		switch ( opt ) 
		{
			case 'f':
				file_name = optarg;
				pFile = fopen( file_name.c_str(), "rb" );
				if ( pFile == NULL ) {
					PRINT_ERROR(" Cannot open file with binary data " );
				}
				break;
			case 'e':
				if( !strcmp(optarg, "fp") ) fp = true;
				else if( !strcmp(optarg, "fn")) fn = true;
				else
					PRINT_ERROR("Udentified error parameter");
				break;
			case 'n':
				classifier_name = optarg;
				Classifier::init();
				//FlowBased::init();
				//CFGBased::init();
				if( !strcmp(optarg, "DisasLength") ) current = new DisasLength();
				else if ( !strcmp(optarg, "PushCall") ) current = new PushCall();
				else if ( !strcmp(optarg, "DataFlowAnomaly") ) current = new DataFlowAnomaly();
				else if ( !strcmp(optarg, "DisasOffset") ) current  = new DisasOffset();
				else if ( !strcmp(optarg, "RaceWalk") ) current = new RaceWalk();
				else if ( !strcmp(optarg, "HDD") ) current = new HDD(0);
				else if ( !strcmp(optarg, "CycleFinder") ) current = new CycleFinder();
				else 
				{
					PRINT_ERROR("no such classifier exists");
					CFGBased::release();
					abort();
				}
				break;
            case 'm':
                mal_file = optarg;
                mFile = fopen(mal_file.c_str(), "rb");
                if ( mFile == NULL )
                {
                    PRINT_ERROR("Can't open file with binary data");
                }
                break;
			case 'h':
				printHelp();
				break;
			default:
				abort();
				
		}
	}

	if (pFile == NULL)
	{
		PRINT_ERROR("No filename is given");
		return 1;
	}
	
	if(current == NULL)
	{
		PRINT_ERROR("No classifier name is given");
		return 1;
	}
	
	int mal_cnt = 0;
	int leg_cnt = 0;
	
	while(!feof(pFile))
	{
		int cnt = fread( buffer, sizeof( unsigned char ), (MAX_DATA_LEN-1)*sizeof( unsigned char ), pFile );

		if ( cnt < MAX_DATA_LEN ) buffer[cnt]='\0';

		Classifier::reload( buffer, cnt );
		FlowBased::reload();
        try {
            CFGBased::reload();
        }
        catch(int x)
        {
            continue;
        }
		
		gettimeofday(&startTime, NULL);
		bool mal = false;
		current->update();
		mal  = current->check();
		gettimeofday(&endTime, NULL);
		timeval_subtract(&diff, &endTime, &startTime);
		timeval_addition(&totalTime, &totalTime, &diff);

		if(mal) 
		{
			mal_cnt++;
		}
		else
		{
			leg_cnt++;
		}
	}

	cout<< "malware: " << mal_cnt << endl << "legitimate: " << leg_cnt << endl; 
	cout << "Time: " << totalTime.tv_sec << " " << totalTime.tv_usec <<std::endl << endl;
	delete current;
	CFGBased::release();
	fclose(pFile);
	
	return 0;
}